/*
 *  The scanner definition for COOL.
 */

/*
 *  Stuff enclosed in %{ %} in the first section is copied verbatim to the
 *  output, so headers and global definitions are placed here to be visible
 * to the code in the file.  Don't remove anything that was here initially
 */
%{
#include <cool-parse.h>
#include <stringtab.h>
#include <utilities.h>

/* The compiler assumes these identifiers. */
#define yylval cool_yylval
#define yylex  cool_yylex

/* Max size of string constants */
#define MAX_STR_CONST 1025
#define YY_NO_UNPUT   /* keep g++ happy */

extern FILE *fin; /* we read from this file */
static int old_state = 0;
static int comment_depth = 0;

/* define YY_INPUT so we read from the FILE fin:
 * This change makes it possible to use this scanner in
 * the Cool compiler.
 */
#undef YY_INPUT
#define YY_INPUT(buf,result,max_size) \
	if ( (result = fread( (char*)buf, sizeof(char), max_size, fin)) < 0) \
		YY_FATAL_ERROR( "read() in flex scanner failed");

char string_buf[MAX_STR_CONST]; /* to assemble string constants */
char *string_buf_ptr;
static char* str_unterm = "Unterminated string constant";
static char* str_escnul = "String contains escaped null character.";
static char* str_nul = "String contains null character.";

extern int curr_lineno;
extern int verbose_flag;

extern YYSTYPE cool_yylval;

int clean_string (int* less, bool* too_long) {
  char * mystr;
  bool escape_slash = false;
  int run_slash = 0;
  // printf("clean start: >>%s<<\n", yytext);
  mystr=strdup(yytext);
  if (! mystr) return ERROR;
  if ((int)yyleng != (int)strlen(mystr)) {
    cool_yylval.error_msg = str_nul;
    BEGIN(0);
    return (ERROR);
  }
  if (yyleng>=1) {
    mystr[yyleng-1]='\0'; // remove trailing "
  } else {
    cool_yylval.error_msg = "wtf";
    free(mystr);
    return (ERROR);
  }
  for (int i=0, j=0; i<=(int)strlen(mystr); i++, j++) { // "<=" and not "<" to get /0
    // printf("head mystr[i] %c escape_slash %d\n", mystr[i], escape_slash);
    if (j >= MAX_STR_CONST) {
      cool_yylval.error_msg = "String constant too long";
      // printf("A\n");
      *less = i-2;
      *too_long = true;
      free(mystr);
      return (ERROR);
    }
    if (mystr[i]=='\\') {
      run_slash++;
      yytext[j]=mystr[i];
      i++;
      if (mystr[i]=='n') {
        yytext[j]='\n';
      } else if (mystr[i]=='t'){
        yytext[j]='\t';
      } else if (mystr[i]=='b'){
        yytext[j]='\b';
      } else if (mystr[i]=='f'){
        yytext[j]='\f';
      } else if (mystr[i]=='\\'){
        escape_slash = !escape_slash;
        run_slash++;
        // printf("escaped slash %d next mystr[i] %c\n", escape_slash, mystr[i+1]);
        // printf("yytext .%s. first %d valid\n", yytext, j+1);
      } else if (mystr[i]=='\0'){
        cool_yylval.error_msg = str_unterm;
        free(mystr);
        yyleng = j;
        // printf("unterminated: .%s.\n", mystr);
        BEGIN(0);
        return (ERROR);
      } else if (mystr[i]=='\n')  {
        // i--;
        // printf("escaped newline\n");
        curr_lineno++;
        yytext[j]='\n';
      }
      else if (isprint(mystr[i]) || 1) {
        yytext[j]=mystr[i];
      } else {
        cool_yylval.error_msg = "unprintable";
        free(mystr);
        return (ERROR);
      }
      if (mystr[i]!='\\') {
        // escape_slash = false;
        run_slash = 0;
      }
    } else {
      if (mystr[i]=='\n') {
        // printf("string newline\n");
        curr_lineno++;
        // printf("huh escape_slash %d mystr[i-1] %c\n", escape_slash, mystr[i-1]);
        if (i == 0 || (mystr[i-1] != '\\') || escape_slash) {
          // printf("abort\n");
          cool_yylval.error_msg = str_unterm;
          free(mystr);
          // printf("unterminated: .%s.\n", yytext);
          // printf("B\n");
          *less = i;
          BEGIN(0);
          return (ERROR);
        } else {
          escape_slash = false;
          yytext[j]=mystr[i];
        }
      } else if (mystr[i]=='\0') {
        // printf("remaining %d\n", (int)strlen(mystr) - i);
        yytext[j]=mystr[i];
        break;
      } else if (mystr[i] == '\"') {
        // printf("quote i %d mystr[i-1] %c run_slash %d\n", i, mystr[i-1], run_slash);
        if ((i > 0) && mystr[i-1] == '\\' && (run_slash%2)) {
          // printf("quote escaped\n");
          // yytext[j]=mystr[i];
        } else {
          *less = i+1;
          yytext[j] = '\0';
          yyleng = j;
          BEGIN(0);
          // printf("end here\n");
          return (STR_CONST);
        }
      } else if (isprint(mystr[i])){
        yytext[j]=mystr[i];
      } else {
        yytext[j]=mystr[i];
        // cool_yylval.error_msg = "unprintable";
        // free(mystr);
        // return (ERROR);
      }
      run_slash = 0;
    }
  }
  yyleng=strlen(yytext);
  free(mystr);
  // printf("clean: >>%s<<\n", yytext);
  return (STR_CONST);
}

void comment_end_action() {
  comment_depth--;
  // printf("comment end .%s. %d\n", yytext, comment_depth);
  if (!comment_depth) {
    BEGIN(0);
  }
} 
/*
 *  Add Your own definitions here
 */

%}


%option noyywrap
%s LINE_COMMENTED
%s COMMENTED
%s QUOTED
%s TOO_LONG
/*
 * Define names for regular expressions here.
 */

DARROW          =>
ASSIGN          <-
CLASS (?i:class)
ELSE (?i:else)
FI (?i:fi)
IF (?i:if)
IN (?i:in)
INHERITS (?i:inherits)
LET (?i:let)
LOOP (?i:loop)
POOL (?i:pool)
THEN (?i:then)
WHILE (?i:while)
CASE (?i:case)
ESAC (?i:esac)
OF (?i:of)
NEW (?i:new)
ISVOID (?i:isvoid)
INT_CONST [0-9]+
TYPEID [A-Z][[:alnum:]|_]*
OBJECTID [a-z][[:alnum:]|_]*
NOT (?i:not)
LE <=
BOOL_TRUE t(?i:rue)
BOOL_FALSE f(?i:alse)
WHITESPACE [\b\t\f\r\v ]
CLEAN_WHITESPACE [\b|\t|\f| ]
OLD_ERROR ([.]{-}[\b\t\n\f [:alnum:]{OTHER_SN}])+
ERROR [^\b\t\n\f ]
START_COMMENT \(\*
END_COMMENT \*\)
OTHER_STR_CHARS [!#$%^&_]
STR_CONST ((\\[^n0])|[[:alnum:]]|{OTHER_STR_CHARS}|{OTHER_SN}|{CLEAN_WHITESPACE})*
OTHER_STR_CONST [^(\\\n|EOF|\\0)]*
OTHER_SN [{};:,\.()<=\+\-~@\*/]
OLD_STR_CONST ([[:alnum:]]|\\\"|\\|{OTHER_SN}|{WHITESPACE})*
%%

 /*
  *  Nested comments
  */


 /*
  *  The multiple-character operators.
  */
<INITIAL>{DARROW}		{ return (DARROW); }
<INITIAL>{ASSIGN}		{ return (ASSIGN); }
<INITIAL>{LOOP} {return (LOOP);}
<INITIAL>{POOL} {return (POOL);}
<INITIAL>{CLASS} {return (CLASS);}
<INITIAL>{ELSE} {return (ELSE);}
<INITIAL>{FI} {return (FI);}
<INITIAL>{IF} {return (IF);}
<INITIAL>{IN} {return (IN);}
<INITIAL>{INHERITS} {return (INHERITS);}
<INITIAL>{LET} {return (LET);}
<INITIAL>{THEN} {return (THEN);}
<INITIAL>{WHILE} {return (WHILE);}
<INITIAL>{CASE} {return (CASE);}
<INITIAL>{ESAC} {return (ESAC);}
<INITIAL>{OF} {return (OF);}
<INITIAL>{NEW} {return (NEW);}
<INITIAL>{ISVOID} {return (ISVOID);}
<INITIAL>{INT_CONST} {
  cool_yylval.symbol = inttable.add_string(yytext);
  return (INT_CONST);
}
<INITIAL>{BOOL_TRUE} {
  cool_yylval.boolean = true;
  return (BOOL_CONST);}
<INITIAL>{BOOL_FALSE} {
  cool_yylval.boolean = false;
  return (BOOL_CONST);}
<INITIAL>{NOT} {return (NOT);}
<INITIAL>{TYPEID} {
  cool_yylval.symbol = idtable.add_string(yytext);
  return (TYPEID);
}
<INITIAL>{OBJECTID} {
  cool_yylval.symbol = idtable.add_string(yytext);
  return (OBJECTID);
}
<INITIAL>{LE} {return (LE);}

<INITIAL>\" {
  // printf("begin quoted\n");
  BEGIN(QUOTED);
}

<QUOTED>([^\"]|(\\\"))*\" {
  int less = -1;
  bool too_long = false;
  // printf("quoted: .%s.\n", yytext);
  int ret = clean_string(&less, &too_long);
  if (less != -1) {
    // yytext[less] = '\0';
    // printf("eating only %s\n", yytext);
    yyless(less);
  }
  // printf("ret %d %s\n", ret, cool_yylval.error_msg);
  if (ret == STR_CONST) {
    cool_yylval.symbol = stringtable.add_string(yytext);
  }
  if (too_long) {
    // printf("too long!\n");
    BEGIN(TOO_LONG);
  } else {
    BEGIN(0);
  }
  return ret;
}
<QUOTED>([^\"]|(\\\")|\0)* {
  // printf("uhh .%s.\n", yytext);
  // snprintf(yytext, yylen, "%s");
  // printf("strlen %d yylen %d\n", );

  cool_yylval.error_msg = "EOF in string constant";
  if ((int)strlen(yytext)!=(int)yyleng) {
    cool_yylval.error_msg = str_nul;
    BEGIN(0);
    for (int i = 0; i < yyleng; i++) {
      if (yytext[i]=='\n') {
        yyless(i);
        // curr_lineno++;
        break;
      }
    }
  }
  return (ERROR);
}
<TOO_LONG>\" {
  BEGIN(INITIAL);
  // printf("discard .%s.\n", yytext);
}
<TOO_LONG>. {
  // printf("baddiscard .%s.\n", yytext);
}
<INITIAL>"--" {
  // printf("line comment begin .%s.\n", yytext);
  BEGIN(LINE_COMMENTED);
}
<LINE_COMMENTED>\n {
  curr_lineno++;
  BEGIN(0);
}
<LINE_COMMENTED>.* {
  // printf("line comment rest .%s.\n", yytext);
  BEGIN(0);
}
<INITIAL,COMMENTED>"\(\*" {
  comment_depth++;
  // printf("comment begin .%s.\n", yytext);
  BEGIN(COMMENTED);
}

<COMMENTED>"\*\)" {
  comment_end_action();
}
<COMMENTED>(\*[^\)]|[^\*\(])+ {
  // printf("#%d skipping .%s.\n", curr_lineno, yytext);
  bool flag = false;
  for (int i = 0; i < yyleng; i++) {
    if (yytext[i] == '*') {
      flag = true;
    } else if (flag && yytext[i] == ')') {
      yyless(i-1);
      // comment_end_action();
      flag = false;
    } else {
      flag = false;
      if (yytext[i] == '\n') {
        // printf("commented newline\n");
        curr_lineno++;
      }
    }
  }
}

<COMMENTED><<EOF>> {
  cool_yylval.error_msg = "EOF";
  BEGIN(0);
  return ERROR;
}
<COMMENTED>. {
  // printf("commented .%s.\n", yytext);
}
<INITIAL>"\*\)" {
  cool_yylval.error_msg = "Unmatched *)";
  return (ERROR);
}


\n {
  // printf("uncommented newline\n");
  curr_lineno++;
} /* advance line counter */
<INITIAL>{WHITESPACE}+ {
  // printf("whitespace .%s.\n", yytext);
} 
<INITIAL>{OTHER_SN} {
  return *yytext;
}
<INITIAL>{ERROR} {
  // printf("baaad error\n");
  cool_yylval.error_msg = yytext;
  // yyterminate();
  return (ERROR);
}
 /*
  * Keywords are case-insensitive except for the values true and false,
  * which must begin with a lower-case letter.
  */


 /*
  *  String constants (C syntax)
  *  Escape sequence \c is accepted for all characters c. Except for 
  *  \n \t \b \f, the result is c.
  *
  */


%%
