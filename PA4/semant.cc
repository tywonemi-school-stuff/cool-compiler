#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include "semant.h"
#include "utilities.h"

static ClassTable *classtable = NULL;
static Store *store = NULL;
static SignatureStore *methods = NULL;
static Class_ g_cur_class = NULL;
extern int semant_debug;
extern char *curr_filename;

static std::ostringstream nop_sstream;
static bool TESTING = false;
static std::ostream &dbg = TESTING ? std::cout : nop_sstream;

//////////////////////////////////////////////////////////////////////
//
// Symbols
//
// For convenience, a large number of symbols are predefined here.
// These symbols include the primitive type and method names, as well
// as fixed names used by the runtime system.
//
//////////////////////////////////////////////////////////////////////
static Symbol
    arg,
    arg2,
    Bool,
    concat,
    cool_abort,
    copy,
    Int,
    in_int,
    in_string,
    IO,
    length,
    Main,
    main_meth,
    No_class,
    No_type,
    Object,
    out_int,
    out_string,
    prim_slot,
    self,
    SELF_TYPE,
    Str,
    str_field,
    substr,
    type_name,
    val;

#include "vibe.cc"
//
// Initializing the predefined symbols.
//
static void initialize_constants(void)
{
    arg = idtable.add_string("arg");
    arg2 = idtable.add_string("arg2");
    Bool = idtable.add_string("Bool");
    concat = idtable.add_string("concat");
    cool_abort = idtable.add_string("abort");
    copy = idtable.add_string("copy");
    Int = idtable.add_string("Int");
    in_int = idtable.add_string("in_int");
    in_string = idtable.add_string("in_string");
    IO = idtable.add_string("IO");
    length = idtable.add_string("length");
    Main = idtable.add_string("Main");
    main_meth = idtable.add_string("main");
    //   _no_class is a symbol that can't be the name of any
    //   user-defined class.
    No_class = idtable.add_string("_no_class");
    No_type = idtable.add_string("_no_type");
    Object = idtable.add_string("Object");
    out_int = idtable.add_string("out_int");
    out_string = idtable.add_string("out_string");
    prim_slot = idtable.add_string("_prim_slot");
    self = idtable.add_string("self");
    SELF_TYPE = idtable.add_string("SELF_TYPE");
    Str = idtable.add_string("String");
    str_field = idtable.add_string("_str_field");
    substr = idtable.add_string("substr");
    type_name = idtable.add_string("type_name");
    val = idtable.add_string("_val");
}

int ClassTable::find_class(Symbol s)
{
    for (int i = 0; i < (int)maybe_classes.size(); i++)
    {
        if (maybe_classes[i]->get_name() == s)
        {
            return i;
        }
    }
    return -1;
}

bool ClassTable::inherits_from(Symbol child, Symbol parent)
{
    int i = find_class(child);
    if (i == -1)
    {
        return false;
    }
    auto c = maybe_classes[i];
    while (true)
    {
        if (c->get_name() == parent)
            return true;
        if (c->get_name() == Object)
            return false;
        i = find_class(c->get_parent());
        if (i != -1)
        {
            c = maybe_classes[i];
        }
        else
        {
            return false;
        }
    }
}

Symbol ClassTable::lub(Symbol one, Symbol other)
{
    if ((one == No_type) || (other == No_type))
    {
        return No_type;
    }
    if (!type_exists(one) || !type_exists(other))
    {
        return No_type;
    }
    int i = find_class(one);
    if (i == -1)
    {
        return NULL;
    }
    auto c = maybe_classes[i];
    while (true)
    {
        if (inherits_from(other, c->get_name()))
            return c->get_name();
        if (c->get_name() == Object)
            return Object;
        int i = find_class(c->get_parent());
        if (i != -1)
        {
            c = maybe_classes[i];
        }
        else
        {
            return NULL;
        }
    }
}

bool ClassTable::type_exists(Symbol type) { return find_class(type) != -1; }

bool ClassTable::is_acyclic(std::vector<int> mark, int start_elem, int iter)
{
    Symbol parent = maybe_classes[start_elem]->get_parent();
    // dbg << "Is " << v[start_elem]->get_name() << " acyclic?" << std::endl;
    if (maybe_classes[start_elem]->get_name() == Object)
    {
        return true;
    }
    int i = find_class(parent);
    mark[i] == iter;
    for (;;)
    {
        if (parent == Object)
        {
            // dbg << "We reached the top";
            return true;
        }
        if ((i == -1) || (parent == Int) || (parent == Bool) || (parent == Str))
        {
            // dbg << "That class doesn't exist, throw err" << std::endl;
            return false;
        }
        if (mark[i] == iter)
        {
            // dbg << "We were here already in this search" << std::endl;
            return false;
        }
        mark[i] = iter;
        parent = maybe_classes[i]->get_parent();
        i = find_class(parent);
    }
    // unreachable
    assert(0);
    return false;
}

ClassTable::ClassTable(Classes classes) : semant_errors(0), error_stream(cerr)
{
    for (int i = classes->first(); classes->more(i); i = classes->next(i))
    {
        if (find_class(classes->nth(i)->get_name()) != -1)
        {
            semant_error(classes->nth(i));
            error_stream << "Class " << classes->nth(i)->get_name() << " redeclared" << std::endl;
            // return;
        }
        maybe_classes.push_back(classes->nth(i));
    }
    install_basic_classes();
    auto mark = std::vector<int>(maybe_classes.size(), -1);
    bool have_main = false;
    for (int i = 0; i < (int)maybe_classes.size(); i++)
    {
        bool ok = is_acyclic(mark, i, i);
        if (maybe_classes[i]->get_name() == Main)
        {
            have_main = true;
        }
        if (!ok)
        {
            semant_error(classes->nth(i));
            error_stream << "Class warfare detected" << std::endl;
            return;
        }
    }
    if (!have_main)
    {
        semant_error();
        error_stream << "Class Main is not defined." << std::endl;
        return;
    }
}

void ClassTable::install_basic_classes()
{

    // The tree package uses these globals to annotate the classes built below.
    // curr_lineno  = 0;
    Symbol filename = stringtable.add_string("<basic class>");

    // The following demonstrates how to create dummy parse trees to
    // refer to basic Cool classes.  There's no need for method
    // bodies -- these are already built into the runtime system.

    // IMPORTANT: The results of the following expressions are
    // stored in local variables.  You will want to do something
    // with those variables at the end of this method to make this
    // code meaningful.

    //
    // The Object class has no parent class. Its methods are
    //        abort() : Object    aborts the program
    //        type_name() : Str   returns a string representation of class name
    //        copy() : SELF_TYPE  returns a copy of the object
    //
    // There is no need for method bodies in the basic classes---these
    // are already built in to the runtime system.

    Class_ Object_class =
        class_(Object,
               No_class,
               append_Features(
                   append_Features(
                       single_Features(method(cool_abort, nil_Formals(), Object, no_expr())),
                       single_Features(method(type_name, nil_Formals(), Str, no_expr()))),
                   single_Features(method(copy, nil_Formals(), SELF_TYPE, no_expr()))),
               filename);
    maybe_classes.push_back(Object_class);
    skip_classes.push_back(Object_class);

    //
    // The IO class inherits from Object. Its methods are
    //        out_string(Str) : SELF_TYPE       writes a string to the output
    //        out_int(Int) : SELF_TYPE            "    an int    "  "     "
    //        in_string() : Str                 reads a string from the input
    //        in_int() : Int                      "   an int     "  "     "
    //
    Class_ IO_class =
        class_(IO,
               Object,
               append_Features(
                   append_Features(
                       append_Features(
                           single_Features(method(out_string, single_Formals(formal(arg, Str)),
                                                  SELF_TYPE, no_expr())),
                           single_Features(method(out_int, single_Formals(formal(arg, Int)),
                                                  SELF_TYPE, no_expr()))),
                       single_Features(method(in_string, nil_Formals(), Str, no_expr()))),
                   single_Features(method(in_int, nil_Formals(), Int, no_expr()))),
               filename);
    maybe_classes.push_back(IO_class);
    skip_classes.push_back(IO_class);

    //
    // The Int class has no methods and only a single attribute, the
    // "val" for the integer.
    //
    Class_ Int_class =
        class_(Int,
               Object,
               single_Features(attr(val, prim_slot, no_expr())),
               filename);
    maybe_classes.push_back(Int_class);
    skip_classes.push_back(Int_class);

    //
    // Bool also has only the "val" slot.
    //
    Class_ Bool_class =
        class_(Bool, Object, single_Features(attr(val, prim_slot, no_expr())), filename);
    maybe_classes.push_back(Bool_class);
    skip_classes.push_back(Bool_class);

    //
    // The class Str has a number of slots and operations:
    //       val                                  the length of the string
    //       str_field                            the string itself
    //       length() : Int                       returns length of the string
    //       concat(arg: Str) : Str               performs string concatenation
    //       substr(arg: Int, arg2: Int): Str     substring selection
    //
    Class_ Str_class =
        class_(Str,
               Object,
               append_Features(
                   append_Features(
                       append_Features(
                           append_Features(
                               single_Features(attr(val, Int, no_expr())),
                               single_Features(attr(str_field, prim_slot, no_expr()))),
                           single_Features(method(length, nil_Formals(), Int, no_expr()))),
                       single_Features(method(concat,
                                              single_Formals(formal(arg, Str)),
                                              Str,
                                              no_expr()))),
                   single_Features(method(substr,
                                          append_Formals(single_Formals(formal(arg, Int)),
                                                         single_Formals(formal(arg2, Int))),
                                          Str,
                                          no_expr()))),
               filename);
    maybe_classes.push_back(Str_class);
    skip_classes.push_back(Str_class);
}

////////////////////////////////////////////////////////////////////
//
// semant_error is an overloaded function for reporting errors
// during semantic analysis.  There are three versions:
//
//    ostream& ClassTable::semant_error()
//
//    ostream& ClassTable::semant_error(Class_ c)
//       print line number and filename for `c'
//
//    ostream& ClassTable::semant_error(Symbol filename, tree_node *t)
//       print a line number and filename
//
///////////////////////////////////////////////////////////////////

ostream &ClassTable::semant_error(Class_ c)
{
    return semant_error(c->get_filename(), c);
}

ostream &ClassTable::semant_error(Symbol filename, tree_node *t)
{
    error_stream << filename << ":" << t->get_line_number() << ": ";
    return semant_error();
}

ostream &ClassTable::semant_error()
{
    semant_errors++;
    return error_stream;
}

/*   This is the entry point to the semantic checker.

     Your checker should do the following two things:

     1) Check that the program is semantically correct
     2) Decorate the abstract syntax tree with type information
        by setting the `type' field in each Expression node.
        (see `tree.h')

     You are free to first do 1), make sure you catch all semantic
     errors. Part 2) can be done in a second stage, when you want
     to build mycoolc.
 */

void program_class::semant()
{
    // dbg << "This is semantic analysis!" << std::endl;
    initialize_constants();

    /* ClassTable constructor may do some semantic analysis */
    classtable = new ClassTable(classes);
    auto all_classes = classtable->maybe_classes;
    store = new Store(classtable);
    store->push(self, SELF_TYPE);
    methods = new SignatureStore(classtable);
    // methods->set_classes(all_classes);
    for (auto cur_class : all_classes)
    {
        auto features = cur_class->get_features();
        for (int j = features->first(); features->more(j); j = features->next(j))
        {
            auto feature = features->nth(j);
            auto feature_name = feature->get_name();
            auto formals = feature->get_formals();
            auto return_type = feature->get_return();
            // dbg << "Inspecting feature " << feature->get_name() << std::endl;
            if (formals && return_type)
            {
                // dbg << "Declared method " << feature_name << " with formals";
                std::vector<Symbol> args = {};
                for (int k = formals->first(); formals->more(k); k = formals->next(k))
                {
                    args.push_back(formals->nth(k)->get_type());
                    // dbg << " " << formals->nth(k)->get_type();
                }
                // dbg << " and return type " << return_type << std::endl;
                signature_t sig = {feature_name, args, return_type};
                methods->declare_method(cur_class->get_name(), feature_name, &sig);
                // bool declare_method(Symbol class_, Symbol method, signature_t* sig);
            }
        }
    }
    methods->detect_overrides();
    // methods->dump();
    if (classtable->errors())
    {
        cerr << "Compilation halted due to static semantic errors." << endl;
        exit(1);
    }
    dbg << "Let's go typecheck!\n";
    for (auto cur_class : all_classes)
    {
        bool skip = false;
        for (auto skipped : classtable->skip_classes)
        {
            if (cur_class == skipped)
            {
                skip = true;
                break;
            }
            if (cur_class->get_name() == skipped->get_name())
            {
                classtable->semant_error(cur_class->get_filename(), cur_class);
                cerr << "Class named reserved type: " << cur_class->get_name() << std::endl;
            }
        }
        if (!skip && ((cur_class->get_name() == Object) || (cur_class->get_name() == SELF_TYPE)))
        {
            classtable->semant_error(cur_class->get_filename(), cur_class);
            cerr << "Class named reserved type: " << cur_class->get_name() << std::endl;
            skip = true;
        }
        if (skip)
            continue;
        g_cur_class = cur_class;
        dbg << "Typechecking class " << g_cur_class->get_name() << std::endl;
        int num_attrs = store->push_attrs(cur_class);
        auto features = cur_class->get_features();
        for (int j = features->first(); features->more(j); j = features->next(j))
        {
            auto feature = features->nth(j);
            if (feature->get_name() == self)
            {
                continue;
            }
            dbg << "Typechecking feature " << feature->get_name() << std::endl;
            feature->vibe_check();
        }
        // dbg << "pop " << num_attrs << std::endl;
        store->pop(num_attrs);
    }
    if (classtable->errors())
    {
        cerr << "Compilation halted due to static semantic errors." << endl;
        exit(1);
    }
}

Symbol Store::get_type(Symbol id)
{
    for (int i = (int)storage.size() - 1; i >= 0; i--)
    {
        if (storage[i].name == id)
            return storage[i].type;
    }
    return NULL;
}

void Store::push(Symbol id, Symbol type)
{
    storage.push_back({id, type});
}
void Store::pop(int count)
{
    if ((int)storage.size() >= count)
    {
        storage.resize(storage.size() - count);
    }
    else
    {
        errs.semant_error();
        cerr << "Store died real badly" << std::endl;
    }
}
int Store::push_attrs(Class_ cl)
{
    int i = classtable->find_class(cl->get_name());
    int num_attrs = 0;
    off_limits.resize(0);
    while (true)
    {
        auto c = classtable->maybe_classes[i];
        if (c->get_name() == Object)
        {
            break;
        }
        auto features = c->get_features();
        for (int j = features->first(); features->more(j); j = features->next(j))
        {
            auto feature = features->nth(j);
            auto type = feature->get_type();
            if (feature->get_name() == self)
            {
                classtable->semant_error(g_cur_class->get_filename(), cl);
                cerr << "Attribute named self" << std::endl;
                continue;
            }
            if (type)
            {
                if (type == SELF_TYPE)
                {
                    type = g_cur_class->get_name();
                }
                if (c != cl)
                {
                    off_limits.push_back(feature->get_name());
                    dbg << "off limits " << feature->get_name() << std::endl;
                }
                push(feature->get_name(), type);
                num_attrs++;
                dbg << "push " << feature->get_name() << std::endl;
            }
        }
        i = classtable->find_class(c->get_parent());
    }
    return num_attrs;
}
bool Store::attr_is_allowed(Symbol id)
{
    dbg << "Checking if allowed " << id << std::endl;
    for (auto banned : off_limits)
    {
        dbg << "Checking banned " << banned << std::endl;
        if (banned == id)
            return false;
    }
    dbg << "Not banned" << std::endl;
    return true;
}

bool SignatureStore::declare_method(Symbol class_, Symbol method, signature_t *sig)
{
    for (int i = 0; i < (int)classes.size(); i++)
    {
        if (classes[i]->get_name() == class_)
        {
            // TODO: check if not redeclared
            for (auto s : storage[i])
            {
                // dbg << (int *)&s << std::endl;
                // dbg << s.fun_name << std::endl;
                if (s.fun_name == method)
                {
                    // error_stream << "Redeclared method " << class_ << "." << method << std::endl;
                    // semant_errors++;
                    return false;
                }
            }
            storage[i].push_back(*sig);
        }
        // return storage[i].type;
    }
    return true;
}
signature_t *SignatureStore::get_signature(Symbol class_, Symbol method)
{
    int i = classtable->find_class(class_);
    if (i == -1)
        return NULL;
    while (true)
    {
        for (int j = 0; j < (int)storage[i].size(); j++)
        {
            if (storage[i][j].fun_name == method)
            {
                return &(storage[i][j]); // this code sucks I hate it
            }
        }
        if (classtable->maybe_classes[i]->get_name() == Object)
        {
            return NULL;
        }
        i = classtable->find_class(classtable->maybe_classes[i]->get_parent());
    }
    return NULL;
}
SignatureStore::SignatureStore(ClassTable *ct) : errs(*ct)
{
    classes = ct->maybe_classes;
    for (int i = 0; i < (int)classes.size(); i++)
    {
        // Initialize for each class its signature vector with no signatures
        std::vector<signature_t> v = {};
        storage.push_back(v);
    }
};
void SignatureStore::detect_overrides()
{
    for (int i = 0; i < (int)storage.size(); i++)
    {
        auto class_ = classes[i];
        if (class_->get_name() == Object)
            continue;
        for (int j = 0; j < (int)storage[i].size(); j++)
        {
            signature_t one = storage[i][j];
            signature_t *other = methods->get_signature(class_->get_parent(), storage[i][j].fun_name);
            if (other)
            {
                bool ok = true;
                if ((other->ret_type != one.ret_type) || (one.arg_types.size() != other->arg_types.size()))
                {
                    ok = false;
                }
                for (int k = 0; k < (int)one.arg_types.size(); k++)
                {
                    if (one.arg_types[k] != other->arg_types[k])
                    {
                        ok = false;
                        break;
                    }
                }
                if (!ok)
                {
                    classtable->semant_error(class_->get_filename(), class_);
                    cerr << "Overriding parent class method with different signature" << std::endl;
                    return;
                }
            }
        }
    }
}
void SignatureStore::dump()
{
    for (int i = 0; i < (int)classes.size(); i++)
    {
        dbg << "Class " << classes[i]->get_name() << std::endl;
        for (auto v : storage[i])
        {
            dbg << "method " << v.fun_name << " has arg types";
            for (auto a : v.arg_types)
            {
                dbg << " " << a;
            }
            dbg << " and returns a " << v.ret_type << std::endl;
        }
    }
}