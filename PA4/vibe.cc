Symbol maybe_self(Symbol type)
{
    if (type == SELF_TYPE)
    {
        return g_cur_class->get_name();
    }
    else
    {
        return type;
    }
}

Symbol dispatch(Expression expr, Symbol type_name, Symbol name, Expressions actual, tree_node *this_node)
{
    auto type = No_type;
    auto signature = methods->get_signature(type_name, name);
    auto expr_type = expr->vibe_check();
    if (!classtable->inherits_from(maybe_self(expr_type), type_name))
    {
        classtable->semant_error(g_cur_class->get_filename(), this_node);
        cerr << "Can't call methods of " << type_name << " on " << maybe_self(expr_type) << std::endl;
    }
    else if (!signature)
    {
        classtable->semant_error(g_cur_class->get_filename(), this_node);
        cerr << "Calling undeclared function " << name << std::endl;
    }
    else
    {
        if ((int)signature->arg_types.size() != (int)actual->len())
        {
            classtable->semant_error(g_cur_class->get_filename(), this_node);
            cerr << "Wrong number of arguments" << std::endl;
        }
        else
        {
            for (int i = 0; i < (int)signature->arg_types.size(); i++)
            {
                auto actual_type = actual->nth(i)->vibe_check();
                if (!classtable->inherits_from(maybe_self(actual_type), signature->arg_types[i]))
                {
                    classtable->semant_error(g_cur_class->get_filename(), this_node);
                    cerr << "Wrong argument type: " << signature->arg_types[i] << " should be " << maybe_self(actual_type) << std::endl;
                    break;
                }
            }
        }
        type = signature->ret_type;
    }
    dbg << "expr " << expr_type << " type " << type << " type_name " << type_name << std::endl;
    if (type == SELF_TYPE) {
        type = expr_type;
    }
    return type;
}

Symbol method_class::vibe_check()
{
    int num_attrs = 0;
    for (int k = formals->first(); formals->more(k); k = formals->next(k))
    {
        auto f = formals->nth(k);
        // check if we're not redefining
        for (int l = k + 1; formals->more(l); l = formals->next(l))
        {
            auto other = formals->nth(l);
            if (f->get_name() == other->get_name())
            {
                classtable->semant_error(g_cur_class->get_filename(), this);
                cerr << "Duplicate arguments named " << f->get_name() << std::endl;
                store->pop(num_attrs);
                return No_type;
            }
        }
        if (f->get_name() == self)
        {
            classtable->semant_error(g_cur_class->get_filename(), this);
            cerr << "Formal named self" << std::endl;
            continue;
        }
        if (classtable->type_exists(f->get_type()))
        {
            store->push(f->get_name(), f->get_type());
            num_attrs++;
        }
        else
        {
            classtable->semant_error(g_cur_class->get_filename(), this);
            cerr << "Invalid type " << f->get_type() << std::endl;
        }
    }
    auto body_type = expr->vibe_check();
    // if (classtable->type_exists(maybe_self(body_type))) {
    // dbg << body_type << return_type << classtable->inherits_from(body_type, maybe_self(return_type)) << std::endl;
    if (return_type == SELF_TYPE)
    {
        if (body_type != SELF_TYPE)
        {
            classtable->semant_error(g_cur_class->get_filename(), this);
            cerr << "SELF_TYPE mismatch in method declaration" << std::endl;
        }
    }
    else if (!classtable->inherits_from(maybe_self(body_type), return_type))
    {
        classtable->semant_error(g_cur_class->get_filename(), this);
        cerr << "Expression " << maybe_self(body_type) << " doesn't conform to " << return_type << std::endl;
    }
    // } else {
    //     classtable->semant_error(g_cur_class->get_filename(), this);
    //     cerr << "Type doesn't exist:" << maybe_self(return_type) << std::endl;

    // }

    // check the existence of the types
    store->pop(num_attrs);
    return return_type;
}
Symbol attr_class::vibe_check()
{
    if (init->lives() && (!classtable->inherits_from(maybe_self(init->vibe_check()), type_decl)))
    {
        classtable->semant_error(g_cur_class->get_filename(), this);
        cerr << "attr " << name << " : " << maybe_self(init->vibe_check()) << " doesn't inherit from " << type_decl << std::endl;
    }
    if (!store->attr_is_allowed(name))
    {
        classtable->semant_error(g_cur_class->get_filename(), this);
        cerr << "attr " << name << ": can't override parent class attribute " << std::endl;
    }
    return No_type;
}

Symbol assign_class::vibe_check()
{
    auto declared_type = store->get_type(name);
    if (declared_type)
    {
        auto expr_type = expr->vibe_check();
        if (!classtable->inherits_from(expr_type, declared_type))
        {
            classtable->semant_error(g_cur_class);
            cerr << "Can't store a " << expr_type << " in " << declared_type << std::endl;
            return No_type;
        }
        type = expr_type;
        return type;
    }
    else
    {
        classtable->semant_error(g_cur_class->get_filename(), this);
        cerr << "Assignment to undeclared identified " << name << std::endl;
        return No_type;
    }
}
Symbol static_dispatch_class::vibe_check()
{
    return type = dispatch(expr, type_name, name, actual, this);
}
Symbol dispatch_class::vibe_check()
{
    return type = dispatch(expr, maybe_self(expr->vibe_check()), name, actual, this);
}
Symbol cond_class::vibe_check()
{
    if (pred->vibe_check() != Bool)
    {
        classtable->semant_error(g_cur_class->get_filename(), this);
        cerr << "Condition is not a bool" << std::endl;
        return No_type;
    }
    auto then_type = then_exp->vibe_check();
    auto else_type = else_exp->vibe_check();
    if (then_type == SELF_TYPE)
        then_type = g_cur_class->get_name();
    if (else_type == SELF_TYPE)
        else_type = g_cur_class->get_name();
    auto lub = classtable->lub(then_type, else_type);
    if (lub == No_type)
    {
        classtable->semant_error(g_cur_class->get_filename(), this);
        // this->dump_with_types(cerr, 0);
        cerr << "Broken lub of " << then_type << " and " << else_type << " is " << lub << std::endl;
    }
    type = lub;
    return type;
}
Symbol loop_class::vibe_check()
{
    if (pred->vibe_check() != Bool)
    {
        classtable->semant_error(g_cur_class->get_filename(), this);
        cerr << "Condition is not a bool" << std::endl;
    }
    body->vibe_check();
    type = Object;
    return type;
}
Symbol typcase_class::vibe_check()
{
    type = NULL;
    std::vector<Symbol> seen_types;
    expr->vibe_check();
    for (int i = cases->first(); cases->more(i); i = cases->next(i))
    {

        auto case_ = cases->nth(i);
        if (!classtable->type_exists(case_->get_type()))
        {
            classtable->semant_error(g_cur_class->get_filename(), this);
            cerr << "Invalid type " << case_->get_type() << std::endl;
            type = No_type;
            break;
        }
        for (auto seen : seen_types)
        {
            if (seen == case_->get_type())
            {
                classtable->semant_error(g_cur_class->get_filename(), this);
                cerr << "Duplicate branches for type " << case_->get_type() << std::endl;
                type = No_type;
                return type;
            }
        }
        seen_types.push_back(case_->get_type());
        store->push(case_->get_name(), case_->get_type());
        auto case_type = case_->get_expr()->vibe_check();
        store->pop(1);
        if (type)
        {
            type = classtable->lub(type, case_type);
        }
        else
        {
            type = case_type;
        }
    }
    return type;
}
Symbol block_class::vibe_check()
{
    type = No_type;
    for (int k = body->first(); body->more(k); k = body->next(k))
    {
        type = body->nth(k)->vibe_check();
    }
    return type;
}
Symbol let_class::vibe_check()
{
    // let self is err
    if (identifier == self)
    {
        classtable->semant_error(g_cur_class->get_filename(), this);
        cerr << "Let self" << std::endl;
        return No_type;
    }
    else
    {
        auto init_type = init->vibe_check();
        if (init->lives() && (type_decl == SELF_TYPE))
        {
            if (init_type != SELF_TYPE)
            {
                classtable->semant_error(g_cur_class->get_filename(), this);
                cerr << "SELF_TYPE mismatch in let" << std::endl;
            }
        }
        else if (init->lives() && (!classtable->inherits_from(init_type, type_decl)))
        {
            classtable->semant_error(g_cur_class->get_filename(), this);
            cerr << "Init type " << init_type << " doesn't conform to " << type_decl << std::endl;
            return No_type;
        }
        if ((classtable->type_exists(type_decl)) || (type_decl == SELF_TYPE))
        {
            store->push(identifier, type_decl);
            type = body->vibe_check();
            store->pop(1);
        }
    }
    return type;
}
Symbol plus_class::vibe_check()
{
    type = No_type;
    auto type_left = e1->vibe_check();
    auto type_right = e2->vibe_check();
    if ((type_left != No_type) && (type_right != No_type))
    {
        if ((type_left == Int) && (type_right == Int))
        {
            type = Int;
        }
        else
        {
            classtable->semant_error(g_cur_class->get_filename(), this);
            cerr << "Can't add " << type_left << " and " << type_right << std::endl;
        }
    }
    return type;
}
Symbol sub_class::vibe_check()
{
    type = No_type;
    auto type_left = e1->vibe_check();
    auto type_right = e2->vibe_check();
    if ((type_left != No_type) && (type_right != No_type))
    {
        if ((type_left == Int) && (type_right == Int))
        {
            type = Int;
        }
        else
        {
            classtable->semant_error(g_cur_class->get_filename(), this);
            cerr << "Can't subtract " << type_left << " and " << type_right << std::endl;
        }
    }
    return type;
}
Symbol mul_class::vibe_check()
{
    type = No_type;
    auto type_left = e1->vibe_check();
    auto type_right = e2->vibe_check();
    if ((type_left != No_type) && (type_right != No_type))
    {
        if ((type_left == Int) && (type_right == Int))
        {
            type = Int;
        }
        else
        {
            classtable->semant_error(g_cur_class->get_filename(), this);
            cerr << "Can't multiply " << type_left << " and " << type_right << std::endl;
        }
    }
    return type;
}
Symbol divide_class::vibe_check()
{
    type = No_type;
    auto type_left = e1->vibe_check();
    auto type_right = e2->vibe_check();
    if ((type_left != No_type) && (type_right != No_type))
    {
        if ((type_left == Int) && (type_right == Int))
        {
            type = Int;
        }
        else
        {
            classtable->semant_error(g_cur_class->get_filename(), this);
            cerr << "Can't divide " << type_left << " and " << type_right << std::endl;
        }
    }
    return type;
}
Symbol neg_class::vibe_check()
{
    auto type_left = e1->vibe_check();
    if (type_left != Int)
    {
        classtable->semant_error(g_cur_class->get_filename(), this);
        cerr << "Can't negate non-integer " << type_left << std::endl;
    }
    type = Int;
    return type;
}
Symbol lt_class::vibe_check()
{
    type = No_type;
    auto type_left = e1->vibe_check();
    auto type_right = e2->vibe_check();
    if ((type_left != No_type) && (type_right != No_type))
    {
        if ((type_left == Int) && (type_right == Int))
        {
            type = Bool;
        }
        else
        {
            classtable->semant_error(g_cur_class->get_filename(), this);
            cerr << "Can't compare " << type_left << " and " << type_right << std::endl;
        }
    }
    return type;
}
Symbol eq_class::vibe_check()
{
    type = No_type;
    auto type_left = e1->vibe_check();
    auto type_right = e2->vibe_check();
    if ((type_left != No_type) && (type_right != No_type))
    {

        if ((type_left == type_right) && ((type_left == Str) || (type_left == Int) || (type_left == Bool)))
        {
            // built-in value comparison
            type = Bool;
        }
        else if ((type_left != Str) && (type_left != Int) && (type_left != Bool) && ((type_right != Str) && (type_right != Int) && (type_right != Bool)))
        {
            // non-built-in object pointer comparison
            type = Bool;
        }
        else
        {
            classtable->semant_error(g_cur_class->get_filename(), this);
            cerr << "Can't compare " << type_left << " and " << type_right << std::endl;
        }
    }
    return type;
}
Symbol leq_class::vibe_check()
{
    type = No_type;
    auto type_left = e1->vibe_check();
    auto type_right = e2->vibe_check();
    if ((type_left != No_type) && (type_right != No_type))
    {
        if ((type_left == Int) && (type_right == Int))
        {
            type = Bool;
        }
        else
        {
            classtable->semant_error(g_cur_class->get_filename(), this);
            cerr << "Can't compare " << type_left << " and " << type_right << std::endl;
        }
    }
    return type;
}
Symbol comp_class::vibe_check()
{
    auto type_left = e1->vibe_check();
    if (type_left != Bool)
    {
        classtable->semant_error(g_cur_class->get_filename(), this);
        cerr << "Can't complement non-boolean " << type_left << std::endl;
    }
    type = Bool;
    return type;
}
Symbol int_const_class::vibe_check()
{
    type = Int;
    return type;
}
Symbol bool_const_class::vibe_check()
{
    type = Bool;
    return type;
}
Symbol string_const_class::vibe_check()
{
    type = Str;
    return type;
}
Symbol new__class::vibe_check()
{
    type = No_type;
    if ((classtable->type_exists(type_name)) || (type_name == SELF_TYPE))
    {
        type = type_name;
    }

    return type;
}
Symbol isvoid_class::vibe_check()
{
    e1->vibe_check();
    type = Bool;
    return type;
}
Symbol no_expr_class::vibe_check() { return No_type; }
Symbol object_class::vibe_check()
{
    type = No_type;
    auto t = store->get_type(name);
    if (t)
    {
        type = t;
    }
    return type;
}
