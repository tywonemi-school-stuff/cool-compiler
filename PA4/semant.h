#ifndef SEMANT_H_
#define SEMANT_H_

#include <assert.h>
#include <iostream>  
#include "cool-tree.h"
#include "stringtab.h"
#include "symtab.h"
#include "list.h"
#include <vector>

#define TRUE 1
#define FALSE 0

class ClassTable;
typedef ClassTable *ClassTableP;
// This is a structure that may be used to contain the semantic
// information such as the inheritance graph.  You may use it or not as
// you like: it is only here to provide a container for the supplied
// methods.

class ClassTable {
private:
  int semant_errors;
  void install_basic_classes();
  ostream& error_stream;
  bool is_acyclic(std::vector<int> mark, int start_elem, int iter);

public:
  ClassTable(Classes);
  int errors() { return semant_errors; }
  ostream& semant_error();
  ostream& semant_error(Class_ c);
  ostream& semant_error(Symbol filename, tree_node *t);
  std::vector<Class_> skip_classes;
  std::vector<Class_> maybe_classes;
  int find_class(Symbol s);
  bool inherits_from(Symbol child, Symbol parent);
  Symbol lub(Symbol one, Symbol other);
  bool type_exists(Symbol type);
};

typedef struct store_elem {
  Symbol name;
  Symbol type;
} store_elem_t;

typedef struct signature {
  Symbol fun_name;
  std::vector<Symbol> arg_types;
  Symbol ret_type;
} signature_t;

class Store {
  private:
    std::vector<store_elem_t> storage;
    std::vector<Symbol> off_limits;
    ClassTable errs;
  public:
    int push_attrs(Class_ cl);
    bool attr_is_allowed(Symbol id);
    Symbol get_type(Symbol id);
    void push(Symbol id, Symbol type);
    void pop(int count);
    Store(ClassTable* ct) : errs(*ct) {};
};

class SignatureStore {
  // M = vector of funlist, funlist is a vector
  private:
    std::vector<std::vector<signature_t>> storage = {};
    std::vector<Class_> classes; 
    ClassTable errs;
  public:
    void dump();
    bool declare_method(Symbol class_, Symbol method, signature_t* sig);
    signature_t* get_signature(Symbol class_, Symbol method);
    void detect_overrides();
    SignatureStore(ClassTable* ct);

};

#endif

