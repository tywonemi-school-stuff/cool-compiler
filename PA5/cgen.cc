
//**************************************************************
//
// Code generator SKELETON
//
// Read the comments carefully. Make sure to
//    initialize the base class tags in
//       `CgenClassTable::CgenClassTable'
//
//    Add the label for the dispatch tables to
//       `IntEntry::code_def'
//       `StringEntry::code_def'
//       `BoolConst::code_def'
//
//    Add code to emit everyting else that is needed
//       in `CgenClassTable::code'
//
//
// The files as provided will produce code to begin the code
// segments, declare globals, and emit constants.  You must
// fill in the rest.
//
//**************************************************************

#include "cgen.h"
#include "cgen_gc.h"
#include <vector>
#include <stack>
#include <set>

extern void emit_string_constant(ostream& str, char *s);
extern int cgen_debug;
#define DBG if (cgen_debug) cout
#define ENV_DEBUG false
static int dumb_counter = 0;

static std::deque<std::pair<Symbol, int> > env;
static int envOffset;
static std::map<Symbol, std::map<Symbol, std::tuple<Symbol, Feature, int> > > dispatchMap;
static std::map<Symbol, std::map<Symbol, std::pair<Feature, int> > > attrMap;
static std::map<Symbol, int> tagMap;
static std::map<Symbol, int> depthMap;
static std::map<Symbol, std::vector<Symbol> > childMap;
static std::map<Symbol, Symbol> parentMap;
static Symbol ctime_self_type;
static void emit_get_intval(ostream &s);
//
// Three symbols from the semantic analyzer (semant.cc) are used.
// If e : No_type, then no code is generated for e.
// Special code is generated for new SELF_TYPE.
// The name "self" also generates code different from other references.
//
//////////////////////////////////////////////////////////////////////
//
// Symbols
//
// For convenience, a large number of symbols are predefined here.
// These symbols include the primitive type and method names, as well
// as fixed names used by the runtime system.
//
//////////////////////////////////////////////////////////////////////
Symbol 
       arg,
       arg2,
       Bool,
       concat,
       cool_abort,
       copy,
       Int,
       in_int,
       in_string,
       IO,
       length,
       Main,
       main_meth,
       No_class,
       No_type,
       Object,
       out_int,
       out_string,
       prim_slot,
       self,
       SELF_TYPE,
       Str,
       str_field,
       substr,
       type_name,
       val;
//
// Initializing the predefined symbols.
//
static void initialize_constants(void)
{
  arg         = idtable.add_string("arg");
  arg2        = idtable.add_string("arg2");
  Bool        = idtable.add_string("Bool");
  concat      = idtable.add_string("concat");
  cool_abort  = idtable.add_string("abort");
  copy        = idtable.add_string("copy");
  Int         = idtable.add_string("Int");
  in_int      = idtable.add_string("in_int");
  in_string   = idtable.add_string("in_string");
  IO          = idtable.add_string("IO");
  length      = idtable.add_string("length");
  Main        = idtable.add_string("Main");
  main_meth   = idtable.add_string("main");
//   _no_class is a symbol that can't be the name of any 
//   user-defined class.
  No_class    = idtable.add_string("_no_class");
  No_type     = idtable.add_string("_no_type");
  Object      = idtable.add_string("Object");
  out_int     = idtable.add_string("out_int");
  out_string  = idtable.add_string("out_string");
  prim_slot   = idtable.add_string("_prim_slot");
  self        = idtable.add_string("self");
  SELF_TYPE   = idtable.add_string("SELF_TYPE");
  Str         = idtable.add_string("String");
  str_field   = idtable.add_string("_str_field");
  substr      = idtable.add_string("substr");
  type_name   = idtable.add_string("type_name");
  val         = idtable.add_string("_val");
}

static char *gc_init_names[] =
  { "_NoGC_Init", "_GenGC_Init", "_ScnGC_Init" };
static char *gc_collect_names[] =
  { "_NoGC_Collect", "_GenGC_Collect", "_ScnGC_Collect" };


//  BoolConst is a class that implements code generation for operations
//  on the two booleans, which are given global names here.
BoolConst falsebool(FALSE);
BoolConst truebool(TRUE);

//*********************************************************
//
// Define method for code generation
//
// This is the method called by the compiler driver
// `cgtest.cc'. cgen takes an `ostream' to which the assembly will be
// emmitted, and it passes this and the class list of the
// code generator tree to the constructor for `CgenClassTable'.
// That constructor performs all of the work of the code
// generator.
//
//*********************************************************

void program_class::cgen(ostream &os) 
{
  // spim wants comments to start with '#'
  os << "# start of generated code\n";

  initialize_constants();
  CgenClassTable *codegen_classtable = new CgenClassTable(classes,os);

  os << "\n# end of generated code\n";
}


//////////////////////////////////////////////////////////////////////////////
//
//  emit_* procedures
//
//  emit_X  writes code for operation "X" to the output stream.
//  There is an emit_X for each opcode X, as well as emit_ functions
//  for generating names according to the naming conventions (see emit.h)
//  and calls to support functions defined in the trap handler.
//
//  Register names and addresses are passed as strings.  See `emit.h'
//  for symbolic names you can use to refer to the strings.
//
//////////////////////////////////////////////////////////////////////////////

static void emit_load(char *dest_reg, int offset, char *source_reg, ostream& s)
{
  s << LW << dest_reg << " " << offset * WORD_SIZE << "(" << source_reg << ")" 
    << endl;
}

static void emit_store(char *source_reg, int offset, char *dest_reg, ostream& s)
{
  s << SW << source_reg << " " << offset * WORD_SIZE << "(" << dest_reg << ")"
      << endl;
}

static void emit_load_imm(char *dest_reg, int val, ostream& s)
{ s << LI << dest_reg << " " << val << endl; }

static void emit_load_address(char *dest_reg, char *address, ostream& s)
{ s << LA << dest_reg << " " << address << endl; }

static void emit_partial_load_address(char *dest_reg, ostream& s)
{ s << LA << dest_reg << " "; }

static void emit_load_bool(char *dest, const BoolConst& b, ostream& s)
{
  emit_partial_load_address(dest,s);
  b.code_ref(s);
  s << endl;
}

static void emit_load_string(char *dest, StringEntry *str, ostream& s)
{
  emit_partial_load_address(dest,s);
  str->code_ref(s);
  s << endl;
}

static void emit_load_int(char *dest, IntEntry *i, ostream& s)
{
  emit_partial_load_address(dest,s);
  i->code_ref(s);
  s << endl;
}

static void emit_move(char *dest_reg, char *source_reg, ostream& s)
{ s << MOVE << dest_reg << " " << source_reg << endl; }

static void emit_neg(char *dest, char *src1, ostream& s)
{ s << NEG << dest << " " << src1 << endl; }

static void emit_add(char *dest, char *src1, char *src2, ostream& s)
{ s << ADD << dest << " " << src1 << " " << src2 << endl; }

static void emit_addu(char *dest, char *src1, char *src2, ostream& s)
{ s << ADDU << dest << " " << src1 << " " << src2 << endl; }

static void emit_addiu(char *dest, char *src1, int imm, ostream& s)
{ s << ADDIU << dest << " " << src1 << " " << imm << endl; }

static void emit_div(char *dest, char *src1, char *src2, ostream& s)
{ s << DIV << dest << " " << src1 << " " << src2 << endl; }

static void emit_mul(char *dest, char *src1, char *src2, ostream& s)
{ s << MUL << dest << " " << src1 << " " << src2 << endl; }

static void emit_sub(char *dest, char *src1, char *src2, ostream& s)
{ s << SUB << dest << " " << src1 << " " << src2 << endl; }

static void emit_sll(char *dest, char *src1, int num, ostream& s)
{ s << SLL << dest << " " << src1 << " " << num << endl; }

static void emit_jalr(char *dest, ostream& s)
{ s << JALR << "\t" << dest << endl; }

static void emit_jal(char *address,ostream &s)
{ s << JAL << address << endl; }

static void emit_return(ostream& s)
{ s << RET << endl; }

static void emit_gc_assign(ostream& s)
{ s << JAL << "_GenGC_Assign" << endl; }

static void emit_disptable_ref(Symbol sym, ostream& s)
{  s << sym << DISPTAB_SUFFIX; }

static void emit_init_ref(Symbol sym, ostream& s)
{ s << sym << CLASSINIT_SUFFIX; }

static void emit_label_ref(char* l, ostream &s)
{ s << l; }

static void emit_protobj_ref(Symbol sym, ostream& s)
{ s << sym << PROTOBJ_SUFFIX; }

static void emit_method_ref(Symbol classname, Symbol methodname, ostream& s)
{ s << classname << METHOD_SEP << methodname; }

static void emit_label_def(char* l, ostream &s)
{
  emit_label_ref(l,s);
  s << ":" << endl;
}

static void emit_beqz(char *source, char* label, ostream &s)
{
  s << BEQZ << source << " ";
  emit_label_ref(label,s);
  s << endl;
}

static void emit_beq(char *src1, char *src2, char* label, ostream &s)
{
  s << BEQ << src1 << " " << src2 << " ";
  emit_label_ref(label,s);
  s << endl;
}

static void emit_bne(char *src1, char *src2, char* label, ostream &s)
{
  s << BNE << src1 << " " << src2 << " ";
  emit_label_ref(label,s);
  s << endl;
}

static void emit_bleq(char *src1, char *src2, char* label, ostream &s)
{
  s << BLEQ << src1 << " " << src2 << " ";
  emit_label_ref(label,s);
  s << endl;
}

static void emit_blt(char *src1, char *src2, char* label, ostream &s)
{
  s << BLT << src1 << " " << src2 << " ";
  emit_label_ref(label,s);
  s << endl;
}

static void emit_blti(char *src1, int imm, char* label, ostream &s)
{
  s << BLT << src1 << " " << imm << " ";
  emit_label_ref(label,s);
  s << endl;
}

static void emit_bgti(char *src1, int imm, char* label, ostream &s)
{
  s << BGT << src1 << " " << imm << " ";
  emit_label_ref(label,s);
  s << endl;
}

static void emit_jump(char* l, ostream& s)
{
  s << BRANCH;
  emit_label_ref(l,s);
  s << endl;
}

//
// Push a register on the stack. The stack grows towards smaller addresses.
//
static void emit_push(char *reg, ostream& str)
{
  emit_store(reg,0,SP,str);
  emit_addiu(SP,SP,-4,str);
  if (ENV_DEBUG) DBG << "push " << reg << endl;
  envOffset++;
}

static void emit_pop(char *reg, ostream& str) {
  emit_addiu(SP,SP,4,str);
  emit_load(reg, 0, SP, str);
  if (ENV_DEBUG) DBG << "pop " << reg << endl;
  envOffset--;
}

/* Top half of a function definition */
static void emit_th(ostream& s) {
  emit_push(FP, s);
  emit_move(FP, SP, s);
  emit_push(RA, s);
  env_push(self, 0);
  emit_push(SELF, s);
}

/* Bottom half of a function definition */
static void emit_bh(int arg_count, ostream& s) {
  env_pop(self);
  emit_pop(SELF, s);
  emit_pop(RA, s);
  emit_pop(FP, s);
  emit_addiu(SP, SP, 4*arg_count, s); // args
  if (ENV_DEBUG) DBG << "......decr " << arg_count << endl;
  envOffset -= arg_count;
  if (ENV_DEBUG) DBG << "that's envOffset " << envOffset << endl;
  emit_return(s);
}

//
// Fetch the integer value in an Int object.
// Emits code to fetch the integer value of the Integer object pointed
// to by register source into the register dest
//
static void emit_fetch_int(char *dest, char *source, ostream& s)
{ emit_load(dest, DEFAULT_OBJFIELDS, source, s); }

//
// Emits code to store the integer value contained in register source
// into the Integer object pointed to by dest.
//
static void emit_store_int(char *source, char *dest, ostream& s)
{ emit_store(source, DEFAULT_OBJFIELDS, dest, s); }


static void emit_test_collector(ostream &s)
{
  emit_push(ACC, s);
  emit_move(ACC, SP, s); // stack end
  emit_move(A1, ZERO, s); // allocate nothing
  s << JAL << gc_collect_names[cgen_Memmgr] << endl;
  emit_addiu(SP,SP,4,s);
  emit_load(ACC,0,SP,s);
}

static void emit_gc_check(char *source, ostream &s)
{
  if (source != (char*)A1) emit_move(A1, source, s);
  s << JAL << "_gc_check" << endl;
}


///////////////////////////////////////////////////////////////////////////////
//
// coding strings, ints, and booleans
//
// Cool has three kinds of constants: strings, ints, and booleans.
// This section defines code generation for each type.
//
// All string constants are listed in the global "stringtable" and have
// type StringEntry.  StringEntry methods are defined both for String
// constant definitions and references.
//
// All integer constants are listed in the global "inttable" and have
// type IntEntry.  IntEntry methods are defined for Int
// constant definitions and references.
//
// Since there are only two Bool values, there is no need for a table.
// The two booleans are represented by instances of the class BoolConst,
// which defines the definition and reference methods for Bools.
//
///////////////////////////////////////////////////////////////////////////////

//
// Strings
//
void StringEntry::code_ref(ostream& s)
{
  s << STRCONST_PREFIX << index;
}

//
// Emit code for a constant String.
// You should fill in the code naming the dispatch table.
//

void StringEntry::code_def(ostream& s, int stringclasstag)
{
  IntEntryP lensym = inttable.add_int(len);

  // Add -1 eye catcher
  s << WORD << "-1" << endl;

  code_ref(s);  s  << LABEL                                             // label
      << WORD << stringclasstag << endl                                 // tag
      << WORD << (DEFAULT_OBJFIELDS + STRING_SLOTS + (len+4)/4) << endl // size
      << WORD;
  emit_disptable_ref(Str, s);


 /***** Add dispatch information for class String ******/

      s << endl;                                              // dispatch table
      s << WORD;  lensym->code_ref(s);  s << endl;            // string length
  emit_string_constant(s,str);                                // ascii string
  s << ALIGN;                                                 // align to word
}

//
// StrTable::code_string
// Generate a string object definition for every string constant in the 
// stringtable.
//
void StrTable::code_string_table(ostream& s, int stringclasstag)
{  
  for (List<StringEntry> *l = tbl; l; l = l->tl())
    l->hd()->code_def(s,stringclasstag);
}

//
// Ints
//
void IntEntry::code_ref(ostream &s)
{
  s << INTCONST_PREFIX << index;
}

//
// Emit code for a constant Integer.
// You should fill in the code naming the dispatch table.
//

void IntEntry::code_def(ostream &s, int intclasstag)
{
  // Add -1 eye catcher
  s << WORD << "-1" << endl;

  code_ref(s);  s << LABEL                                // label
      << WORD << intclasstag << endl                      // class tag
      << WORD << (DEFAULT_OBJFIELDS + INT_SLOTS) << endl  // object size
      << WORD;
  emit_disptable_ref(Int, s);

 /***** Add dispatch information for class Int ******/

      s << endl;                                          // dispatch table
      s << WORD << str << endl;                           // integer value
}


//
// IntTable::code_string_table
// Generate an Int object definition for every Int constant in the
// inttable.
//
void IntTable::code_string_table(ostream &s, int intclasstag)
{
  for (List<IntEntry> *l = tbl; l; l = l->tl())
    l->hd()->code_def(s,intclasstag);
}


//
// Bools
//
BoolConst::BoolConst(int i) : val(i) { assert(i == FALSE || i == TRUE); }

void BoolConst::code_ref(ostream& s) const
{
  s << BOOLCONST_PREFIX << val;
}
  
//
// Emit code for a constant Bool.
// You should fill in the code naming the dispatch table.
//

void BoolConst::code_def(ostream& s, int boolclasstag)
{
  // Add -1 eye catcher
  s << WORD << "-1" << endl;

  code_ref(s);  s << LABEL                                  // label
      << WORD << boolclasstag << endl                       // class tag
      << WORD << (DEFAULT_OBJFIELDS + BOOL_SLOTS) << endl   // object size
      << WORD;
  emit_disptable_ref(Bool, s);

 /***** Add dispatch information for class Bool ******/

      s << endl;                                            // dispatch table
      s << WORD << val << endl;                             // value (0 or 1)
}

//////////////////////////////////////////////////////////////////////////////
//
//  CgenClassTable methods
//
//////////////////////////////////////////////////////////////////////////////

//***************************************************
//
//  Emit code to start the .data segment and to
//  declare the global names.
//
//***************************************************

void CgenClassTable::code_global_data()
{
  Symbol main    = idtable.lookup_string(MAINNAME);
  Symbol string  = idtable.lookup_string(STRINGNAME);
  Symbol integer = idtable.lookup_string(INTNAME);
  Symbol boolc   = idtable.lookup_string(BOOLNAME);

  str << "\t.data\n" << ALIGN;
  //
  // The following global names must be defined first.
  //
  str << GLOBAL << CLASSNAMETAB << endl;
  str << GLOBAL; emit_protobj_ref(main,str);    str << endl;
  str << GLOBAL; emit_protobj_ref(integer,str); str << endl;
  str << GLOBAL; emit_protobj_ref(string,str);  str << endl;
  str << GLOBAL; falsebool.code_ref(str);  str << endl;
  str << GLOBAL; truebool.code_ref(str);   str << endl;
  str << GLOBAL << INTTAG << endl;
  str << GLOBAL << BOOLTAG << endl;
  str << GLOBAL << STRINGTAG << endl;
  str << GLOBAL << "Main.main" << endl;

  //
  // We also need to know the tag of the Int, String, and Bool classes
  // during code generation.
  //
  str << INTTAG << LABEL
      << WORD << intclasstag << endl;
  str << BOOLTAG << LABEL 
      << WORD << boolclasstag << endl;
  str << STRINGTAG << LABEL 
      << WORD << stringclasstag << endl;    
}


//***************************************************
//
//  Emit code to start the .text segment and to
//  declare the global names.
//
//***************************************************

void CgenClassTable::code_global_text()
{
  str << GLOBAL << HEAP_START << endl
      << HEAP_START << LABEL 
      << WORD << 0 << endl
      << "\t.text" << endl
      << GLOBAL;
  emit_init_ref(idtable.add_string("Main"), str);
  str << endl << GLOBAL;
  emit_init_ref(idtable.add_string("Int"),str);
  str << endl << GLOBAL;
  emit_init_ref(idtable.add_string("String"),str);
  str << endl << GLOBAL;
  emit_init_ref(idtable.add_string("Bool"),str);
  str << endl << GLOBAL;
  emit_method_ref(idtable.add_string("Main"), idtable.add_string("main"), str);
  str << endl;
}

void CgenClassTable::code_bools(int boolclasstag)
{
  falsebool.code_def(str,boolclasstag);
  truebool.code_def(str,boolclasstag);
}

void CgenClassTable::code_select_gc()
{
  //
  // Generate GC choice constants (pointers to GC functions)
  //
  str << GLOBAL << "_MemMgr_INITIALIZER" << endl;
  str << "_MemMgr_INITIALIZER:" << endl;
  str << WORD << gc_init_names[cgen_Memmgr] << endl;
  str << GLOBAL << "_MemMgr_COLLECTOR" << endl;
  str << "_MemMgr_COLLECTOR:" << endl;
  str << WORD << gc_collect_names[cgen_Memmgr] << endl;
  str << GLOBAL << "_MemMgr_TEST" << endl;
  str << "_MemMgr_TEST:" << endl;
  str << WORD << (cgen_Memmgr_Test == GC_TEST) << endl;
}


//********************************************************
//
// Emit code to reserve space for and initialize all of
// the constants.  Class names should have been added to
// the string table (in the supplied code, is is done
// during the construction of the inheritance graph), and
// code for emitting string constants as a side effect adds
// the string's length to the integer table.  The constants
// are emmitted by running through the stringtable and inttable
// and producing code for each entry.
//
//********************************************************

void CgenClassTable::code_constants()
{
  //
  // Add constants that are required by the code generator.
  //
  stringtable.add_string("");
  inttable.add_string("0");

  stringtable.code_string_table(str,stringclasstag);
  inttable.code_string_table(str,intclasstag);
  code_bools(boolclasstag);
}


CgenClassTable::CgenClassTable(Classes classes, ostream& s) : nds(NULL) , str(s)
{
  stringclasstag = 0 /* Change to your String class tag here */;
  intclasstag =    1 /* Change to your Int class tag here */;
  boolclasstag =   2 /* Change to your Bool class tag here */;
  startclasstag =  3;
  envOffset = 0;
  enterscope();
  DBG << "Building CgenClassTable" << endl;
  install_basic_classes();
  install_classes(classes);
  build_inheritance_tree();

  build_features();
  build_classtags();

  code();
  exitscope();
}

void CgenClassTable::install_basic_classes()
{

// The tree package uses these globals to annotate the classes built below.
  //curr_lineno  = 0;
  Symbol filename = stringtable.add_string("<basic class>");

//
// A few special class names are installed in the lookup table but not
// the class list.  Thus, these classes exist, but are not part of the
// inheritance hierarchy.
// No_class serves as the parent of Object and the other special classes.
// SELF_TYPE is the self class; it cannot be redefined or inherited.
// prim_slot is a class known to the code generator.
//
  addid(No_class,
	new CgenNode(class_(No_class,No_class,nil_Features(),filename),
			    Basic,this));
  addid(SELF_TYPE,
	new CgenNode(class_(SELF_TYPE,No_class,nil_Features(),filename),
			    Basic,this));
  addid(prim_slot,
	new CgenNode(class_(prim_slot,No_class,nil_Features(),filename),
			    Basic,this));

// 
// The Object class has no parent class. Its methods are
//        cool_abort() : Object    aborts the program
//        type_name() : Str        returns a string representation of class name
//        copy() : SELF_TYPE       returns a copy of the object
//
// There is no need for method bodies in the basic classes---these
// are already built in to the runtime system.
//
  install_class(
   new CgenNode(
    class_(Object, 
	   No_class,
	   append_Features(
           append_Features(
           single_Features(method(cool_abort, nil_Formals(), Object, no_expr())),
           single_Features(method(type_name, nil_Formals(), Str, no_expr()))),
           single_Features(method(copy, nil_Formals(), SELF_TYPE, no_expr()))),
	   filename),
    Basic,this));

// 
// The IO class inherits from Object. Its methods are
//        out_string(Str) : SELF_TYPE          writes a string to the output
//        out_int(Int) : SELF_TYPE               "    an int    "  "     "
//        in_string() : Str                    reads a string from the input
//        in_int() : Int                         "   an int     "  "     "
//
   install_class(
    new CgenNode(
     class_(IO, 
            Object,
            append_Features(
            append_Features(
            append_Features(
            single_Features(method(out_string, single_Formals(formal(arg, Str)),
                        SELF_TYPE, no_expr())),
            single_Features(method(out_int, single_Formals(formal(arg, Int)),
                        SELF_TYPE, no_expr()))),
            single_Features(method(in_string, nil_Formals(), Str, no_expr()))),
            single_Features(method(in_int, nil_Formals(), Int, no_expr()))),
	   filename),	    
    Basic,this));

//
// The Int class has no methods and only a single attribute, the
// "val" for the integer. 
//
   install_class(
    new CgenNode(
     class_(Int, 
	    Object,
            single_Features(attr(val, prim_slot, no_expr())),
	    filename),
     Basic,this));

//
// Bool also has only the "val" slot.
//
    install_class(
     new CgenNode(
      class_(Bool, Object, single_Features(attr(val, prim_slot, no_expr())),filename),
      Basic,this));

//
// The class Str has a number of slots and operations:
//       val                                  ???
//       str_field                            the string itself
//       length() : Int                       length of the string
//       concat(arg: Str) : Str               string concatenation
//       substr(arg: Int, arg2: Int): Str     substring
//       
   install_class(
    new CgenNode(
      class_(Str, 
	     Object,
             append_Features(
             append_Features(
             append_Features(
             append_Features(
             single_Features(attr(val, Int, no_expr())),
            single_Features(attr(str_field, prim_slot, no_expr()))),
            single_Features(method(length, nil_Formals(), Int, no_expr()))),
            single_Features(method(concat, 
				   single_Formals(formal(arg, Str)),
				   Str, 
				   no_expr()))),
	    single_Features(method(substr, 
				   append_Formals(single_Formals(formal(arg, Int)), 
						  single_Formals(formal(arg2, Int))),
				   Str, 
				   no_expr()))),
	     filename),
        Basic,this));

}

// CgenClassTable::install_class
// CgenClassTable::install_classes
//
// install_classes enters a list of classes in the symbol table.
//
void CgenClassTable::install_class(CgenNodeP nd)
{
  Symbol name = nd->get_name();

  if (probe(name))
    {
      return;
    }

  // The class name is legal, so add it to the list of classes
  // and the symbol table.
  nds = new List<CgenNode>(nd,nds);
  addid(name,nd);
}

void CgenClassTable::install_classes(Classes cs)
{
  for(int i = cs->first(); cs->more(i); i = cs->next(i))
    install_class(new CgenNode(cs->nth(i),NotBasic,this));
}

//
// CgenClassTable::build_inheritance_tree
//
void CgenClassTable::build_inheritance_tree()
{
  for(List<CgenNode> *l = nds; l; l = l->tl())
      set_relations(l->hd());
}

//
// CgenClassTable::set_relations
//
// Takes a CgenNode and locates its, and its parent's, inheritance nodes
// via the class table.  Parent and child pointers are added as appropriate.
//
void CgenClassTable::set_relations(CgenNodeP nd)
{
  CgenNode *parent_node = probe(nd->get_parent());
  nd->set_parentnd(parent_node);
  parent_node->add_child(nd);
}

void CgenNode::add_child(CgenNodeP n)
{
  children = new List<CgenNode>(n,children);
}

void CgenNode::set_parentnd(CgenNodeP p)
{
  assert(parentnd == NULL);
  assert(p != NULL);
  parentnd = p;
}



void CgenClassTable::code()
{
  if (cgen_debug) cout << "coding global data" << endl;
  code_global_data();

  if (cgen_debug) cout << "choosing gc" << endl;
  code_select_gc();

//                   - class_nameTab
  code_name_table();

  if (cgen_debug) cout << "coding constants" << endl;
  code_constants();

//                 Add your code to emit
//                   - prototype objects
  code_prototypes();
//                   - dispatch tables
  code_dispatch_table();
//
  code_obj_table();

  if (cgen_debug) cout << "coding global text" << endl;
  code_global_text();

//                 Add your code to emit
//                   - object initializer
  code_initializers();
//                   - the class methods
  code_methods();
//                   - etc...
}


CgenNodeP CgenClassTable::root()
{
   return probe(Object);
}


///////////////////////////////////////////////////////////////////////
//
// CgenNode methods
//
///////////////////////////////////////////////////////////////////////

CgenNode::CgenNode(Class_ nd, Basicness bstatus, CgenClassTableP ct) :
   class__class((const class__class &) *nd),
   parentnd(NULL),
   children(NULL),
   basic_status(bstatus)
{ 
   stringtable.add_string(name->get_string());          // Add class name to string table
}


//******************************************************************
//
//   Fill in the following methods to produce code for the
//   appropriate expression.  You may add or remove parameters
//   as you wish, but if you do, remember to change the parameters
//   of the declarations in `cool-tree.h'  Sample code for
//   constant integers, strings, and booleans are provided.
//
//*****************************************************************

static void env_push(Symbol var, int loc) {
  if (ENV_DEBUG) DBG << "env_push " << var << endl;
  env.push_back(std::make_pair(var, loc - envOffset));
}
static void env_pop(Symbol var) {
  if (ENV_DEBUG) DBG << "env_pop " << var << endl;
  auto p(env.back());
  assert(p.first == var);
  env.pop_back();
}
static int _env_lookup(Symbol var) {
  if (ENV_DEBUG) DBG << "env_lookup " << var << endl;
  int out = -1;
  std::for_each(env.rbegin(), env.rend(), [var, &out](const auto p) -> void {
    if ((p.first == var) && (out == -1)) out = p.second + envOffset;
  });
  return out;
}
/* Sets ACC to point to the desired object */
static void emit_var(Symbol var, ostream &s) {
  // first we try local variables and arguments
  int sp_offset = _env_lookup(var);
  if (sp_offset == -1) {
    // It's going to be an attribute
    auto a = attrMap[ctime_self_type][var];
    int attr_offset = std::get<1>(a);
    s << "# load self." << var << endl;
    emit_addiu(ACC, SELF, 4*(3+attr_offset), s);
    DBG << ">>>> var: Found attr " << var << " offset " << attr_offset << endl;
  } else {
    s << "# load local " << var << endl;
    emit_addiu(ACC, SP, 4*sp_offset, s);
    // DBG << ">>>> var: Found local " << var << " offset " << sp_offset << endl;
  }
}
/* Sets variable to be the object pointed to by ACC */
static void emit_set_var(Symbol var, ostream &s) {
  // DBG << ">>>> set var " << var << endl;
  emit_push(ACC, s);
  emit_var(var, s);
  emit_pop(T1, s);
  s << "# setting var " << var << endl;
  emit_store(T1, 0, ACC, s);
}
/* Sets ACC to point at variable */
static void emit_get_var(Symbol var, ostream &s) {
  // DBG << ">>>> get var " << var << endl;
  emit_var(var, s);
  emit_load(ACC, 0, ACC, s);
}
static void env_clear() {
  env.clear();
  // DBG << "......reset" << endl;
  envOffset = 0;
}

void assign_class::code(ostream &s) {
  expr->code(s);
  emit_set_var(name, s);
  emit_move(ACC, T1, s);
}

static void emit_dispatch (Symbol type, Expression expr, Symbol name, Expressions actual, ostream &s) {
  bool is_static = !!type;
  if (!is_static) {
    type = expr->get_type();
    if (type == SELF_TYPE) {
      type = ctime_self_type;
    }
  }
  s << "# call " << type << "." << name << endl;
  int j;
  Feature _method = std::get<1>(dispatchMap[type][name]);
  for (j = actual->first(); actual->more(j); j = actual->next(j)) {
    Expression argVal = actual->nth(j);
    Symbol argName = _method->get_formals()->nth(j)->get_name();
    s << "# arg " << argName << endl;
    argVal->code(s);
    s << "# push arg " << argName << endl;
    emit_push(ACC, s);
  }
  expr->code(s);
  emit_beq(ZERO, ACC, "disp_abort", s);
  emit_move(SELF, ACC, s);
  if (is_static) {
    emit_partial_load_address(ACC, s);
    s << type << DISPTAB_SUFFIX << endl;
  } else {
    emit_load(ACC, DISPTABLE_OFFSET, ACC, s);
  }
  int offset = std::get<2>(dispatchMap[type][name]);
  assert(offset >= 0);
  // DBG << "call method " << type << "." << name << " at dispTab offset " << offset << endl;
  emit_load(T1, offset, ACC, s);
  // copy dispatch obj to ACC
  emit_move(ACC, SELF, s);
  emit_jalr(T1, s);
  // DBG << "......decr " << j << endl;
  envOffset -= j;
  // save retval to T1
  emit_move(T1, ACC, s);
  s << "# restore self" << endl;
  emit_get_var(self, s);
  emit_move(SELF, ACC, s);
  emit_move(ACC, T1, s);
}
void static_dispatch_class::code(ostream &s) {
  emit_dispatch(type_name, expr, name, actual, s);
}

void dispatch_class::code(ostream &s) {
  emit_dispatch(NULL, expr, name, actual, s);
}

void cond_class::code(ostream &s) {
  int here = dumb_counter++;
  char true_label[32];
  char false_label[32];
  char join_label[32];
  snprintf(true_label, 32, "true%X", here);
  snprintf(false_label, 32, "false%X", here);
  snprintf(join_label, 32, "join%X", here);
  if (ENV_DEBUG) DBG << "PRE-pred envOffset " << envOffset << endl;
  pred->code(s);
  emit_get_intval(s);
  int start_off = envOffset;
  emit_bne(ZERO, ACC, true_label, s);
  if (ENV_DEBUG) DBG << "POST-pred envOffset " << envOffset << endl;
  // body;
  emit_label_def(false_label, s);
  if (ENV_DEBUG) DBG << "PRE-else envOffset " << envOffset << endl;
  else_exp->code(s);
  assert(start_off == envOffset);
  if (ENV_DEBUG) DBG << "POST-else envOffset " << envOffset << endl;
  emit_jump(join_label, s);
  emit_label_def(true_label, s);
  if (ENV_DEBUG) DBG << "PRE-then envOffset " << envOffset << endl;
  then_exp->code(s);
  assert(start_off == envOffset);
  if (ENV_DEBUG) DBG << "POST-then envOffset " << envOffset << endl;
  emit_label_def(join_label, s);
}

void loop_class::code(ostream &s) {
  int here = dumb_counter++;
  char start_label[32];
  char true_label[32];
  char join_label[32];
  snprintf(start_label, 32, "loop_pred%X", here);
  snprintf(true_label, 32, "loop_body%X", here);
  snprintf(join_label, 32, "loop_done%X", here);
  emit_label_def(start_label, s);
  pred->code(s);
  emit_get_intval(s);
  emit_bne(ZERO, ACC, true_label, s);
  emit_jump(join_label, s);
  emit_label_def(true_label, s);
  body->code(s);
  emit_jump(start_label, s);
  emit_label_def(join_label, s);
  emit_move(ACC, ZERO, s);
}

static void emit_children(Symbol type, char* label, ostream &s) {
  for (auto &child: childMap[type]) {
    emit_children(child, label, s);
    emit_load_imm(ACC, tagMap[child], s);
    emit_beq(ACC, T1, label, s);
  }
}
void typcase_class::code(ostream &s) {
  // for each case, get the type
  emit_push(SELF, s);
  expr->code(s);
  emit_move(SELF, ACC, s);
  s << "# case" << endl;
  emit_beq(ZERO, SELF, "abort2", s);
  std::vector<Case> vCases;
  for (int i = cases->first(); cases->more(i); i = cases->next(i)) {
    Case c = cases->nth(i);
    vCases.push_back(c);
  }
  std::sort(vCases.begin(), vCases.end(), [](Case a, Case b) {
    return depthMap[a->get_type()] > depthMap[b->get_type()];
  });
  int start = dumb_counter++;
  int here = start;
  char join_label[32];
  snprintf(join_label, 32, "case_join%X", here);
  // load tag of self
  emit_load(T1, TAG_OFFSET, SELF, s);
  for (auto &vc: vCases) {
    here++;
    char true_label[32];
    snprintf(true_label, 32, "match_%s_%X", vc->get_type()->get_string(), here);
    // DBG << "case:" << vc->get_type() << " " << true_label << endl;
    emit_load_imm(ACC, tagMap[vc->get_type()], s);
    emit_beq(ACC, T1, true_label, s);
    emit_children(vc->get_type(), true_label, s);
  }
  emit_move(ACC, SELF, s);
  emit_jump("_case_abort", s);
  // jump to join
  here = start;
  for (auto &vc: vCases) {
    here++;
    char true_label[32];
    snprintf(true_label, 32, "match_%s_%X", vc->get_type()->get_string(), here);
    emit_label_def(true_label, s);
    env_push(vc->get_name(), 0);
    emit_push(SELF, s);
    vc->get_expr()->code(s);
    env_pop(vc->get_name());
    emit_pop(SELF, s);
    emit_jump(join_label, s);
  }
  emit_label_def(join_label, s);
  emit_pop(SELF, s);
}

void block_class::code(ostream &s) {
  for (int i = body->first(); body->more(i); i = body->next(i)) {
      Expression expr = body->nth(i);
      expr->code(s);
  }
}
static void emit_default_init(Symbol type_decl, ostream &s) {
  if (type_decl == Str) {
    emit_load_address(ACC, "String_protObj", s);
    emit_jal("Object.copy", s);
    emit_jal("String_init", s);
  } else if (type_decl == Int) {
    emit_load_address(ACC, "Int_protObj", s);
    emit_jal("Object.copy", s);
    emit_jal("Int_init", s);
  } else if (type_decl == Bool) {
    emit_load_address(ACC, "Bool_protObj", s);
    emit_jal("Object.copy", s);
    emit_jal("Bool_init", s);
  } else if (type_decl == Object) {
    emit_load_address(ACC, "Object_protObj", s);
    emit_jal("Object.copy", s);
    emit_jal("Object_init", s);
  } else {
    emit_load_imm(ACC, 0, s);
  }
}
void let_class::code(ostream &s) {
  s << "# let " << identifier << endl;
  if (init->is_noexpr()) {
    emit_default_init(type_decl, s);
  } else {
    init->code(s);
  }
  env_push(identifier, 0);
  // DBG << "pushing " << identifier << " with envOffset " << envOffset << endl;
  // s << "# setting identifier " << identifier << endl;
  emit_push(ACC, s);
  body->code(s);
  env_pop(identifier);
  emit_pop(ZERO, s);
}

static void emit_get_intval(ostream &s) {
  emit_load(ACC, 3 + attrMap[Int][val].second, ACC, s);
}
static void emit_set_intval(ostream &s) {
  emit_store(T1, 3 + attrMap[Int][val].second, ACC, s);
}
static void emit_binop_int_th(Expression e1, Expression e2, ostream &s) {
	e1->code(s);
  emit_get_intval(s);
  emit_push(ACC, s);
	e2->code(s);
  emit_get_intval(s);
  emit_pop(T1, s);
}
static void emit_binop_int_bh(ostream &s) {
  emit_push(ACC, s);
  auto res = new__class(Int);
  res.code(s);
  emit_pop(T1, s);
  emit_set_intval(s);
}
static void emit_binop_th(Expression e1, Expression e2, ostream &s) {
	e1->code(s);
  emit_push(ACC, s);
	e2->code(s);
  emit_pop(T1, s);
}

void plus_class::code(ostream &s) {
  emit_binop_int_th(e1, e2, s);
  emit_add(ACC, T1, ACC, s);
  emit_binop_int_bh(s);
}

void sub_class::code(ostream &s) {
  emit_binop_int_th(e1, e2, s);
  emit_sub(ACC, T1, ACC, s);
  emit_binop_int_bh(s);
}

void mul_class::code(ostream &s) {
  emit_binop_int_th(e1, e2, s);
  emit_mul(ACC, T1, ACC, s);
  emit_binop_int_bh(s);
}

void divide_class::code(ostream &s) {
  emit_binop_int_th(e1, e2, s);
  emit_div(ACC, T1, ACC, s);
  emit_binop_int_bh(s);
}

void neg_class::code(ostream &s) {
	e1->code(s);
  emit_get_intval(s);
  emit_sub(ACC, ZERO, ACC, s);
  emit_binop_int_bh(s);
}

#define EMIT_COMPARE_TH(body, final) { \
  emit_binop_int_th(e1, e2, s); \
  int here = dumb_counter++; \
  char true_label[32]; \
  char false_label[32]; \
  char join_label[32]; \
  snprintf(true_label, 32, "true%X", here); \
  snprintf(false_label, 32, "false%X", here); \
  snprintf(join_label, 32, "join%X", here); \
  body; \
  final \
}

#define EMIT_COMPARE_BH { \
  emit_label_def(false_label, s); \
  emit_load_imm(ACC, FALSE, s); \
  emit_jump(join_label, s); \
  emit_label_def(true_label, s); \
  emit_load_imm(ACC, TRUE, s); \
  emit_label_def(join_label, s); \
  emit_push(ACC, s); \
  auto res = new__class(Bool); \
  res.code(s); \
  emit_pop(T1, s); \
  emit_set_intval(s); \
}

void lt_class::code(ostream &s) {
  EMIT_COMPARE_TH(
    emit_blt(T1, ACC, true_label, s),
  EMIT_COMPARE_BH)
}

void eq_class::code(ostream &s) {
  s << "# eq" << endl;
  if (e1->type == Int || e1->type == Str || e1->type == Bool) {
      if (e2->type == Int || e2->type == Str || e2->type == Bool) {
        	e1->code(s);
          emit_push(ACC, s);
          e2->code(s);
          emit_move(T2, ACC, s);
          emit_pop(T1, s);
          emit_load_bool(ACC, BoolConst(1), s);
          emit_load_bool(A1, BoolConst(0), s);
          emit_jal("equality_test", s);
      }
  } else {
    emit_binop_th(e1, e2, s);
    int here = dumb_counter++;
    char true_label[32];
    char false_label[32];
    char join_label[32];
    snprintf(true_label, 32, "true%X", here);
    snprintf(false_label, 32, "false%X", here);
    snprintf(join_label, 32, "join%X", here);
    emit_beq(T1, ACC, true_label, s);
    emit_label_def(false_label, s);
    emit_load_imm(ACC, FALSE, s);
    emit_jump(join_label, s);
    emit_label_def(true_label, s);
    emit_load_imm(ACC, TRUE, s);
    emit_label_def(join_label, s);
    emit_push(ACC, s);
    auto res = new__class(Bool);
    res.code(s);
    emit_pop(T1, s);
    emit_set_intval(s);
  }
}

void leq_class::code(ostream &s) {
  EMIT_COMPARE_TH(
    emit_bleq(T1, ACC, true_label, s),
  EMIT_COMPARE_BH)
}

void comp_class::code(ostream &s) {
  auto e2 = bool_const(FALSE);
  EMIT_COMPARE_TH(
    emit_beq(T1, ACC, true_label, s),
  EMIT_COMPARE_BH)
}

void int_const_class::code(ostream& s)  
{
  //
  // Need to be sure we have an IntEntry *, not an arbitrary Symbol
  //
  emit_load_int(ACC,inttable.lookup_string(token->get_string()),s);
}

void string_const_class::code(ostream& s)
{
  emit_load_string(ACC,stringtable.lookup_string(token->get_string()),s);
}

void bool_const_class::code(ostream& s)
{
  emit_load_bool(ACC, BoolConst(val), s);
}

void new__class::code(ostream &s) {
  s << "# new " << type_name << endl;
  DBG << "new " << type_name << endl;
  if (type_name == SELF_TYPE) {
    // load address of objtab
    emit_load_address(ACC, CLASSOBJTAB, s);
    // load tag of self
    emit_load(T1, TAG_OFFSET, SELF, s);
    // multiply tag by 8 to get offset
    emit_sll(T1, T1, 3, s);
    emit_add(ACC, ACC, T1, s);
    emit_push(ACC, s);
    // put the address of protObj into ACC as arg for copy
    emit_load(ACC, 0, ACC, s);
  } else {
    emit_partial_load_address(ACC, s);
    s << type_name << PROTOBJ_SUFFIX << endl;
  }
  emit_jal("Object.copy", s);
  s << "# set self" << endl;
  emit_move(SELF, ACC, s);
  if (type_name == SELF_TYPE) {
    // put the address of init procedure into T1
    emit_pop(T1, s);
    emit_load(T1, 1, T1, s);
  } else {
    emit_partial_load_address(T1, s);
    s << type_name << CLASSINIT_SUFFIX << endl;
  }
  emit_jalr(T1, s);
  emit_move(T1, ACC, s);
  s << "# restore self" << endl;
  emit_get_var(self, s);
  emit_move(SELF, ACC, s);
  emit_move(ACC, T1, s);
}

void isvoid_class::code(ostream &s) {
  int here = dumb_counter++;
  char true_label[32];
  char false_label[32];
  char join_label[32];
  snprintf(true_label, 32, "true%X", here);
  snprintf(false_label, 32, "false%X", here);
  snprintf(join_label, 32, "join%X", here);
  s << "# is void?" << endl;
  e1->code(s);
  emit_beq(ZERO, ACC, true_label, s),
  emit_label_def(false_label, s);
  emit_load_imm(ACC, FALSE, s);
  emit_jump(join_label, s);
  emit_label_def(true_label, s);
  emit_load_imm(ACC, TRUE, s);
  emit_label_def(join_label, s);
  emit_push(ACC, s);
  auto res = new__class(Bool);
  res.code(s);
  emit_pop(T1, s);
  emit_set_intval(s);
}

void no_expr_class::code(ostream &s) {
  assert(0); // unreachable
}

void object_class::code(ostream &s) {
  s << "# access " << name << endl;
  emit_get_var(name, s);
}


void CgenClassTable::code_prototypes() {

  str << WORD << -1 << endl; // gc
  str << "Int_protObj" << LABEL;
  str << WORD << intclasstag << endl;
  str << WORD << 4 << endl; // size without gc
  str << WORD;
  emit_disptable_ref(Int, str);
  str << endl;
  str << WORD << 0 << endl; // value

  str << WORD << -1 << endl; // gc
  str << "Bool_protObj" << LABEL;
  str << WORD << boolclasstag << endl;
  str << WORD << 4 << endl; // size without gc
  str << WORD;
  emit_disptable_ref(Bool, str);
  str << endl;
  str << WORD << FALSE << endl;

  str << WORD << -1 << endl; // gc
  str << "String_protObj" << LABEL;
  str << WORD << stringclasstag << endl;
  str << WORD << 5 << endl; // size without gc
  str << WORD;
  emit_disptable_ref(Str, str);
  str << endl;
  str << WORD;
  inttable.add_string("0")->code_ref(str);
  str << endl;
  str << WORD << 0 << endl;

  for (List<CgenNode> *l = nds; l; l = l->tl()) {
    Symbol class_name = l->hd()->get_name();
    if (attrMap.count(class_name) != 0) {
      std::vector<Feature> attrs;
      Features features = l->hd()->get_features();
      for (const auto& [attr_name, value] : attrMap[class_name]) {
        int offset = value.second;
        auto attr = value.first;
        if (offset >= (int)attrs.size()) {
          attrs.resize(offset + 1);
        }
        attrs[offset] = attr;
      }
      if ((class_name != Str) && (class_name != Int) && (class_name != Bool)) {
        str << WORD << -1 << endl;
        str << class_name << PROTOBJ_SUFFIX << LABEL;
        str << WORD << tagMap[class_name] << endl; // 0
        str << WORD << attrs.size() + 3 << endl; // 4
        str << WORD << class_name << DISPTAB_SUFFIX << endl; // 8
        for (auto &a: attrs) {
          if ((a->get_type() == prim_slot) || (a->get_type() == SELF_TYPE)) {
            str << WORD << "0" << endl;
          } else {
            // DBG << "class " << class_name << " attr " << m.second << " type " << m.first << endl;
            str << WORD;
            emit_protobj_ref(a->get_type(),str);
            str << endl;
          }
        }
      }
    } else {
      DBG << "missing attribute map for class " << class_name << endl;
    }
  }
}
void CgenClassTable::ensure_features_mapped(CgenNodeP nd) {
  Symbol class_name = nd->get_name();
  if (dispatchMap.count(class_name) == 0) {
    CgenNodeP parent = nd->get_parentnd();
    if (parent) {
      ensure_features_mapped(parent);
      depthMap[class_name] = depthMap[parent->get_name()] + 1;
      parentMap[class_name] = parent->get_name();
      childMap[class_name] = std::vector<Symbol>();
      for(List<CgenNode> *l = nd->get_children(); l; l = l->tl()) {
        childMap[class_name].push_back(l->hd()->get_name());
      }
      auto parent_dmap = dispatchMap[parent->get_name()];
      auto this_dmap(parent_dmap);
      auto parent_amap = attrMap[parent->get_name()];
      auto this_amap(parent_amap);
      Features features = nd->get_features();
      int d_offset = dispatchMap[parent->get_name()].size();
      int a_offset = attrMap[parent->get_name()].size();
      for (int i = features->first(); features->more(i); i = features->next(i)) {
        Feature feature = features->nth(i);
        Symbol name = feature->get_name();
        if (feature->get_type() == NULL) {
          // it's a method
          if (this_dmap.count(name) == 0) {
            // new method
            this_dmap[name] = std::make_tuple(class_name, feature, d_offset);
            d_offset++;
          } else {
            // overriding inherited method
            this_dmap[name] = std::make_tuple(class_name, feature, std::get<2>(this_dmap[name]));
          }
        } else {
          // it's an attribute
          if ((this_amap.count(name) == 0) && (class_name != Str) && (class_name != Int) && (class_name != Bool)) {
            this_amap[name] = std::make_pair(feature, a_offset);
            // DBG << "class " << class_name << " attr " << name << " type " << feature->get_type() << " at offset " << a_offset << endl;
            a_offset++;
          }
        }
      }
      dispatchMap[class_name] = this_dmap;
      attrMap[class_name] = this_amap;
    } else {
      depthMap[class_name] = 0;
    }
  }
}

void CgenClassTable::build_features() {
  attrMap[Int][val] = std::make_pair(attr(val, prim_slot, no_expr()), 0);
  attrMap[Bool][val] = std::make_pair(attr(val, prim_slot, no_expr()), 0);
  attrMap[Str][val] = std::make_pair(attr(val, prim_slot, no_expr()), 0);
  attrMap[Str][str_field] = std::make_pair(attr(str_field, prim_slot, no_expr()), 1);
  for(List<CgenNode> *l = nds; l; l = l->tl()) {
    ensure_features_mapped(l->hd());
  }
  // for(List<CgenNode> *l = nds; l; l = l->tl()) {
  //   auto class_name = l->hd()->get_name();
  //   DBG << "ATTRIBUTE MAP " << class_name << endl;
  //   for (const auto& [attr_name, value] : attrMap[class_name]) {
  //     DBG << attr_name << ":" << std::get<1>(value) << endl;;
  //   }
  // }
}
void CgenClassTable::code_dispatch_table() {
  for (List<CgenNode> *l = nds; l; l = l->tl()) {
    Symbol class_name = l->hd()->get_name();
    if (dispatchMap.count(class_name) != 0) {
      std::vector<std::pair<Symbol, Symbol> > methods;
      Features features = l->hd()->get_features();
      for (const auto& [method_name, value] : dispatchMap[class_name]) {
        int offset = std::get<2>(value);
        if (offset >= (int)methods.size()) {
          methods.resize(offset + 1);
        }
        methods[offset] = std::make_pair(std::get<0>(value), method_name);
      }
      str << class_name << DISPTAB_SUFFIX << LABEL;
      for (auto &m: methods) {
        str << WORD;
        emit_method_ref(m.first, m.second, str);
        str << endl;
      }
    } else {
      DBG << "missing dispatch map for class " << class_name << endl;
    }
  }
}

void CgenClassTable::build_classtags() {
  int i = startclasstag;
  tagMap[Str] = stringclasstag;
  tagMap[Int] = intclasstag;
  tagMap[Bool] = boolclasstag;
  for (List<CgenNode> *l = nds; l; l = l->tl()) {
    Symbol class_name = l->hd()->get_name();
    if ((class_name != Str) && (class_name != Int) && (class_name != Bool)) {
      tagMap[class_name] = i++;
      DBG << "tag for " << class_name << " is " << tagMap[class_name] << endl;
    }
  }
}

void CgenClassTable::code_name_table() {
  std::vector<Symbol> names;
  for (List<CgenNode> *l = nds; l; l = l->tl()) {
    Symbol class_name = l->hd()->get_name();
    int idx = tagMap[class_name];
    if (idx >= (int)names.size()) {
      names.resize(idx + 1);
    }
    names[idx] = class_name;
  }
  str << CLASSNAMETAB << LABEL;
  for (auto &m: names) {
    auto class_name_obj = stringtable.add_string(m->get_string());
    str << WORD;
    class_name_obj->code_ref(str);
    str << endl;
  }
}

void CgenClassTable::code_initializers() {
  for (List<CgenNode> *l = nds; l; l = l->tl()) {
    Symbol class_name = l->hd()->get_name();
    DBG << "Initializing " << class_name << endl;
    str << class_name << CLASSINIT_SUFFIX << LABEL;
    ctime_self_type = class_name; 
    emit_th(str);
    // emit_move(T2, SELF, str); // store self pointer
    for (const auto& [attr_name, value] : attrMap[class_name]) {
      auto attr = value.first;
      auto i = value.second;
      if (!attr) {
        DBG << "No attribute known for " << class_name << "." << attr_name << endl;
      }
      if (attr && (attr->get_type() != prim_slot) && (attr->get_init())) {
        auto attr_init = attr->get_init();
        DBG << "Emitting init for " << attr->get_name() << " idx " << i << endl;
        str << "# default init for " << attr->get_name() << " idx " << i << endl;
        emit_default_init(attr->get_type(), str);
        emit_move(T1, ACC, str);
        emit_get_var(self, str);
        emit_store(T1, 3+i, ACC, str);
      }
    }
    std::stack<std::map<Symbol, std::pair<Feature, int> > > stack;
    auto here_class = class_name;
    while (here_class != Object) {
      stack.push(attrMap[here_class]);
      here_class = parentMap[here_class];
    }
    std::set<int> done;
    std::vector<std::pair<Feature, int> > todo;
    while (!stack.empty()) {
      auto map = stack.top();
      for (const auto& [attr_name, value] : map) {
        auto attr = value.first;
        auto i = value.second;
        if (attr && (attr->get_type() != prim_slot) && (attr->get_init()) && (!done.contains(i))) {
          done.insert(i);
          if (i >= (int)todo.size()) {
            todo.resize(i + 1);
          }
          todo[i] = value;
        }
      }
      stack.pop();
    }
    for (auto & [attr, i]: todo) {
      auto attr_init = attr->get_init();
      DBG << "Emitting proper init for " << attr->get_name() << " at " << i << endl;
      if (!attr->get_init()->is_noexpr()) {
        str << "# proper init for " << attr->get_name() << endl;
        attr_init->code(str);
        emit_move(T1, ACC, str);
        emit_get_var(self, str);
        emit_store(T1, 3+i, ACC, str);
      }
    }
    
    emit_bh(0, str);
    DBG << "Initialized " << class_name << endl;
  }
}

void CgenClassTable::code_obj_table() {
  std::vector<Symbol> names;
  for (List<CgenNode> *l = nds; l; l = l->tl()) {
    Symbol class_name = l->hd()->get_name();
    int idx = tagMap[class_name];
    if (idx >= (int)names.size()) {
      names.resize(idx + 1);
    }
    names[idx] = class_name;
  }
  str << CLASSOBJTAB << LABEL;
  for (auto &m: names) {
    // auto class_name_obj = stringtable.add_string(m->get_string());
    str << WORD;
    emit_protobj_ref(m,str);
    str << endl;
    str << WORD;
    emit_init_ref(m,str);
    str << endl;
  }
}

void CgenClassTable::code_methods() {
  emit_label_def("abort2", str);
  emit_load_address(ACC, "str_const0", str);
  emit_load_imm(T1, 1, str);
  emit_jal("_case_abort2", str);
  emit_label_def("disp_abort", str);
  emit_load_address(ACC, "str_const0", str);
  emit_load_imm(T1, 1, str);
  emit_jal("_dispatch_abort", str);
  for (List<CgenNode> *l = nds; l; l = l->tl()) {
    auto nd = l->hd();
    auto class_name = nd->get_name();
    ctime_self_type = class_name;
    Features features = nd->get_features();
    for (int i = features->first(); features->more(i); i = features->next(i)) {
      Feature feature = features->nth(i);
      Symbol name = feature->get_name();
      if (feature->get_type() == NULL) {
        // it's a method
        if (!feature->get_expr()->is_noexpr()) {
          emit_method_ref(class_name, name, str);
          str << LABEL;
          env_clear();
          DBG << "Emitting code for " << class_name << "." << name << endl;
          Formals formals = feature->get_formals();
          int j;
          std::stack<Symbol> reverse_args;
          for (j = formals->first(); formals->more(j); j = formals->next(j)) {
            auto formal_name = formals->nth(j)->get_name();
            env_push(formal_name, 0);
            envOffset++;
            reverse_args.push(formal_name);
          }
          if (ENV_DEBUG) DBG << "PRE-TH envOffset " << envOffset << endl;
          emit_th(str);
          if (ENV_DEBUG) DBG << "PRE-CGEN envOffset " << envOffset << endl;
          feature->get_expr()->code(str);
          if (ENV_DEBUG) DBG << "POST-CGEN envOffset " << envOffset << endl;
          emit_bh(j, str);
          while (!reverse_args.empty()) {
            Symbol argName = reverse_args.top();
            // DBG << "def reverse arg pop! " << argName << endl;
            env_pop(argName);
            reverse_args.pop();
          }
          if (ENV_DEBUG) DBG << "END envOffset " << envOffset << " j " << j <<  endl;
          assert(envOffset == 0);
        }
      }
    }
  }
}
