#!/bin/fish
make cgen -j12
X=(string join '' $argv[1] ".cl") ./lexer $X | ./parser $X | ./semant $X > typed-ast
./mycoolc (string join '' $argv[1] ".cl") -c
set -x DEFAULT_TRAP_HANDLER (realpath ../../lib/trap.handler)
# spim -exception_file $DEFAULT_TRAP_HANDLER -file (realpath (string join '' $argv[1] ".s")) 
/home/emil/school/comp/newer/bin/spim -file (realpath (string join '' $argv[1] ".s")) 